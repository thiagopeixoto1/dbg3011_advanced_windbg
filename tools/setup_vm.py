# This script allows to build the Debugger VM and Target VM
import sys
import subprocess
import os
import traceback
import shutil
import urllib.request
from pathlib import Path
from datetime import datetime
from ctypes import windll


def show_last_exception():
    """Taken from gef. Let's us see proper backtraces from python exceptions"""
    PYTHON_MAJOR = sys.version_info[0]
    horizontal_line = "-"
    right_arrow = "->"
    down_arrow = "\\->"

    print("")
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print(" Exception raised ".center(80, horizontal_line))
    print("{}: {}".format(exc_type.__name__, exc_value))
    print(" Detailed stacktrace ".center(80, horizontal_line))
    for fs in traceback.extract_tb(exc_traceback)[::-1]:
        if PYTHON_MAJOR == 2:
            filename, lineno, method, code = fs
        else:
            try:
                filename, lineno, method, code = (
                    fs.filename,
                    fs.lineno,
                    fs.name,
                    fs.line,
                )
            except:
                filename, lineno, method, code = fs

        print(
            """{} File "{}", line {:d}, in {}()""".format(
                down_arrow, filename, lineno, method
            )
        )
        print("   {}    {}".format(right_arrow, code))


def add_to_path(name, path):
    old_path = os.environ["PATH"].rstrip(";")
    if path in old_path:
        print(f"{name} already in PATH")
        return

    print(f"Adding {name} to PATH...")
    new_path = old_path + ";" + path
    p = subprocess.run(
        ["setx", "/M", "PATH", new_path], capture_output=True, shell=True
    )
    if p.returncode != 0:
        print(
            f'Failed to add {name} to the PATH. Please manually add "{path}" to the PATH. You may have to restart cmd.exe too. Then restart this script.'
        )
        sys.exit(1)
    os.environ["PATH"] = new_path


# https://www.7-zip.org/download.html
def install_7zip():
    if os.path.isdir("C:\\Program Files\\7-Zip"):
        print("7-Zip already installed")
        return

    print("Installing 7-Zip...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://www.7-zip.org/a/7z2201-x64.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\7z2201-x64.exe",
            ]
        )
        subprocess.call(["C:\\users\\IEUser\\Downloads\\7z2201-x64.exe", "/S"])
    except:
        print("7-Zip install failed")
        show_last_exception()
        sys.exit(1)


# https://notepad-plus-plus.org/downloads/
def install_notepadpp():
    if os.path.isdir("C:\\Program Files\\Notepad++"):
        print("notepad++ already installed")
        return

    print("Installing notepad++...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.5.4/npp.8.5.4.Installer.x64.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\npp.8.5.4.Installer.x64.exe",
            ]
        )
        subprocess.call(
            ["C:\\users\\IEUser\\Downloads\\npp.8.5.4.Installer.x64.exe", "/S"]
        )
    except:
        print("notepad++ install failed")
        show_last_exception()
        sys.exit(1)


def install_firefox():
    if os.path.isdir("C:\\Program Files\\Mozilla Firefox"):
        print("Firefox already installed")
        return

    print("Installing Firefox...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=en-GB",
                "--output",
                "C:\\users\\IEUser\\Downloads\\Firefox Setup Latest.exe",
            ]
        )
        subprocess.call(
            ["C:\\users\\IEUser\\Downloads\\Firefox Setup Latest.exe", "/S"]
        )
    except:
        print("Firefox install failed")
        show_last_exception()
        sys.exit(1)


def install_sysinternals():
    if os.path.isdir("C:\\users\\IEUser\\Desktop\\Tools\\SysinternalsSuite"):
        print("Sysinternals tools already installed")
        return

    print("Installing Sysinternals tools...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://download.sysinternals.com/files/SysinternalsSuite.zip",
                "--output",
                "C:\\users\\IEUser\\Downloads\\SysinternalsSuite.zip",
            ]
        )
        subprocess.call(
            [
                "C:\\Program Files\\7-Zip\\7z.exe",
                "x",
                "C:\\Users\\IEUser\\Downloads\\SysinternalsSuite.zip",
                "-oC:\\Users\\IEUser\\Desktop\\Tools\\SysinternalsSuite",
            ]
        )
    except:
        print("Sysinternals tools install failed")
        show_last_exception()
        sys.exit(1)


# https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-1---enable-the-windows-subsystem-for-linux
def install_wsl():
    installNeeded = False
    try:
        subprocess.run(["wsl", "-e", "id"], capture_output=True)
    except FileNotFoundError:
        installNeeded = True
    if not installNeeded:
        print("WSL already installed")
        return

    print("Installing WSL...")
    try:
        subprocess.call(
            [
                "dism.exe",
                "/online",
                "/enable-feature",
                "/featurename:Microsoft-Windows-Subsystem-Linux",
                "/all",
                "/norestart",
            ]
        )
    except:
        print("WSL install failed")
        show_last_exception()
        sys.exit(1)
    print(
        "WSL installed, please restart your computer and re-run this script to continue"
    )
    sys.exit(1)


# https://docs.microsoft.com/en-us/windows/wsl/install-manual
# https://wiki.ubuntu.com/WSL
def install_ubuntu():
    try:
        stdoutdata = subprocess.getoutput("wsl -e id")
    except:
        print(
            "WSL is required to install Ubuntu. Check WSL installation and try rebooting?"
        )
        return
    if "uid=1000" in stdoutdata:
        print("Ubuntu already installed")
        return

    print("Installing Ubuntu...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://aka.ms/wslubuntu2004",
                "--output",
                "C:\\users\\IEUser\\Downloads\\ubuntu-2004.appx",
            ]
        )
        subprocess.call(
            [
                "powershell",
                "Add-AppxPackage",
                "C:\\users\\IEUser\\Downloads\\ubuntu-2004.appx",
            ]
        )
    except:
        print("Ubuntu install failed")
        show_last_exception()
        sys.exit(1)
    print(
        "Please manually run the following command 'ubuntu' and setup your own Ubuntu username: 'user' and password: 'user'. Later re-run this script to continue installing other software"
    )
    sys.exit(1)


# https://docs.microsoft.com/en-us/powershell/module/defender/add-mppreference?view=win10-ps
def setup_windows_defender():
    setupNeeded = True
    try:
        stdoutdata = subprocess.getoutput("powershell Get-MpPreference")
    except:
        print("ERROR: Can't get Windows Defender setup")
        sys.exit(1)
    for line in stdoutdata.split("\n"):
        if not line.startswith("ExclusionPath"):
            continue
        # Test the last configured excluded path to make sure all of them worked
        if "C:\\Users\\IEUser\\Downloads" in line:
            setupNeeded = False
            break
    if not setupNeeded:
        print("Windows Defender already setup")
        return

    print("Setup Windows Defender...")
    try:
        subprocess.call(
            [
                "powershell",
                "Add-MpPreference",
                "-ExclusionPath",
                "C:\\Users\\IEUser\\Desktop",
            ]
        )
        subprocess.call(
            [
                "powershell",
                "Add-MpPreference",
                "-ExclusionPath",
                "C:\\Users\\IEUser\\Downloads",
            ]
        )
    except:
        print("Windows Defender setup failed")
        show_last_exception()
        sys.exit(1)


def setup_taskmanager():
    try:
        stdoutdata = subprocess.getoutput(
            'reg query "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Image File Execution Options\\taskmgr.exe" /v Debugger'
        )
    except:
        print("Task manager can't be setup, ignoring")
        return
    if "procexp64.exe" in stdoutdata or "PROCEXP64.EXE" in stdoutdata:
        print("Task manager already setup")
        return

    print("Setup Task manager...")
    try:
        subprocess.call(
            [
                "reg",
                "add",
                "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Image File Execution Options\\taskmgr.exe",
                "/v",
                "Debugger",
                "/t",
                "REG_SZ",
                "/d",
                "C:\\Users\\IEUser\\Desktop\\Tools\\SysinternalsSuite\\procexp64.exe",
            ]
        )
    except:
        print("Task manager setup failed")
        show_last_exception()
        sys.exit(1)


# https://processhacker.sourceforge.io/downloads.php
def install_processhacker():
    if os.path.isdir("C:\\Program Files\\Process Hacker 2"):
        print("Process Hacker already installed")
        return

    print("Installing Process Hacker...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://github.com/processhacker/processhacker/releases/download/v2.39/processhacker-2.39-setup.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\processhacker-2.39-setup.exe",
            ]
        )
        subprocess.call(
            ["C:\\users\\IEUser\\Downloads\\processhacker-2.39-setup.exe", "/SILENT"]
        )
    except:
        print("Process Hacker install failed")
        show_last_exception()
        sys.exit(1)


# https://www.codespeedy.com/how-to-check-the-internet-connection-in-python/
def is_connected(host="http://google.com"):
    try:
        urllib.request.urlopen(host)
        return True
    except:
        return False


# https://www.easeus.com/todo-backup-resource/how-to-stop-windows-10-from-automatically-update.html#part4
# https://github.com/vFense/vFenseAgent-win/wiki/Registry-keys-for-configuring-Automatic-Updates-&-WSUS#registry-keys-for-automatic-update-configuration-options
def setup_autoupdate():
    try:
        stdoutdata = subprocess.getoutput(
            'reg query "HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU" /v AUOptions'
        )
    except:
        print("Auto update can't be disabled, ignoring")
        return
    if "AUOptions" in stdoutdata and "0x2" in stdoutdata:
        print("Auto Update already disabled")
        return

    print("Disabling auto update...")
    if is_connected():
        print(
            "You need to have an host-only network interface at first, please delete this VM and restart from scratch."
        )
        sys.exit(1)

    try:
        subprocess.call(
            [
                "reg",
                "add",
                "HKEY_LOCAL_MACHINE\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU",
                "/v",
                "AUOptions",
                "/t",
                "Reg_DWORD",
                "/d",
                "2",
            ]
        )
    except:
        print("Auto update disabling failed")
        show_last_exception()
        sys.exit(1)
    print(
        "Auto Update disabled successfully. You can now change the network interface to NAT and restart this script"
    )
    sys.exit(1)


# https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
# NOTE: If they delete the installer, it can be retrieved from https://drive.google.com/file/d/1lsza9Tryk5BrCpyuH9E9LkU5hBM_Q_gH/view
def install_putty():
    if os.path.isdir("C:\\Program Files\\PuTTY"):
        print("Putty already installed")
        return

    print("Installing Putty...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.78-installer.msi",
                "--output",
                "C:\\users\\IEUser\\Downloads\\putty-64bit-0.78-installer.msi",
            ]
        )
        subprocess.call(
            [
                "msiexec",
                "/i",
                "C:\\users\\IEUser\\Downloads\\putty-64bit-0.78-installer.msi",
                "/quiet",
                "/qn",
            ]
        )
    except:
        print("Putty install failed")
        show_last_exception()
        sys.exit(1)


# https://visualstudio.microsoft.com/downloads/
# https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16#
# https://docs.microsoft.com/en-us/visualstudio/install/use-command-line-parameters-to-install-visual-studio?view=vs-2019
# https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-community?view=vs-2019&preserve-view=true#desktop-development-with-c
def install_visualstudio():
    if os.path.isdir(
        "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community"
    ):
        print("Visual Studio already installed")
        return

    print("Installing Visual Studio...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://download.visualstudio.microsoft.com/download/pr/5c555b0d-fffd-45a2-9929-4a5bb59479a4/7a92444b6df4ad128e5eaf1e787fa6fe0fe8e86ba039e37b98b8be6bcc0ea878/vs_Community.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\vs_Community.exe",
            ]
        )
        subprocess.call(
            [
                "C:\\users\\IEUser\\Downloads\\vs_Community.exe",
                "--wait",
                "--passive",
                "--add",
                "Microsoft.VisualStudio.Workload.NativeDesktop;includeRecommended",
            ]
        )
    except:
        print("Visual Studio install failed")
        show_last_exception()
        sys.exit(1)


GIT_PATH = "C:\\Program Files\\Git\\bin\\"


# https://git-scm.com/download/win
def install_git():
    if os.path.isdir("C:\\Program Files\\Git"):
        print("Git already installed")
        return

    print("Installing Git...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://github.com/git-for-windows/git/releases/download/v2.41.0.windows.2/Git-2.41.0.2-64-bit.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\Git-2.41.0.2-64-bit.exe",
            ]
        )
        subprocess.call(
            ["C:\\users\\IEUser\\Downloads\\Git-2.41.0.2-64-bit.exe", "/SILENT"]
        )
    except:
        print("Git install failed")
        show_last_exception()
        sys.exit(1)


def add_git_to_path():
    add_to_path("git", GIT_PATH)


# https://ghidra-sre.org/
GHIDRA_VERSION = "ghidra_10.3.1_PUBLIC"
GHIDRA_INSTALL_DIR = "C:\\Users\\IEUser\\Desktop\\Tools\\" + GHIDRA_VERSION


# https://github.com/NationalSecurityAgency/ghidra/releases
def install_ghidra():
    javaNeeded = False
    try:
        subprocess.run(["java", "--help"], capture_output=True)
    except FileNotFoundError:
        javaNeeded = True
    if javaNeeded:
        print(
            "Java not installed or not in PATH. You have to install it first. You may have to restart cmd.exe too. Then restart this script."
        )
        return

    if os.path.isdir(GHIDRA_INSTALL_DIR):
        print("Ghidra already installed")
        return

    print("Installing Ghidra...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://github.com/NationalSecurityAgency/ghidra/releases/download/Ghidra_10.3.1_build/ghidra_10.3.1_PUBLIC_20230614.zip",
                "--output",
                "C:\\users\\IEUser\\Downloads\\ghidra_10.3.1_PUBLIC_20230614.zip",
            ]
        )
        subprocess.call(
            [
                "C:\\Program Files\\7-Zip\\7z.exe",
                "x",
                "C:\\Users\\IEUser\\Downloads\\ghidra_10.3.1_PUBLIC_20230614.zip",
                "-oC:\\Users\\IEUser\\Desktop\\Tools\\",
            ]
        )
    except:
        print("Ghidra install failed")
        show_last_exception()
        sys.exit(1)

    # See ghidra_9.2.2_PUBLIC/docs/README_PDB.html to get MSFT symbols working
    cwd = os.getcwd()
    os.chdir(
        "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\DIA SDK\\bin\\amd64"
    )
    subprocess.call(["regsvr32", "msdia140.dll"])
    os.chdir(cwd)


def check_windbg():
    if not os.path.isfile(
        "C:\\Users\\IEUser\\AppData\\Local\\Microsoft\\WindowsApps\\WinDbgX.exe"
    ):
        print(
            "WinDbg Preview is not installed. Please install it manually from the Microsoft Store"
        )
        sys.exit(1)


def setup_symbols():
    if os.environ.get("_NT_SYMBOL_PATH") is None:
        print("Setup symbols, will need a reboot")

    subprocess.run(
        [
            "setx",
            "/M",
            "_NT_SYMBOL_PATH",
            "SRV*c:\symbols*http://msdl.microsoft.com/download/symbols",
        ],
        capture_output=True,
    )


# https://www.vim.org/download.php#pc
def install_gvim():
    if os.path.isdir("C:\\Program Files (x86)\\Vim"):
        print("gvim already installed")
        return

    print("Installing gvim...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://ftp.nluug.nl/pub/vim/pc/gvim90.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\gvim90.exe",
            ]
        )
        subprocess.call(["C:\\users\\IEUser\\Downloads\\gvim90.exe", "/S"])
    except:
        print("gvim install failed")
        show_last_exception()
        sys.exit(1)


# https://winmerge.org/?lang=en
def install_winmerge():
    if os.path.isdir("C:\\Program Files\\WinMerge"):
        print("WinMerge already installed")
        return

    print("Installing WinMerge...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://github.com/WinMerge/winmerge/releases/download/v2.16.30/WinMerge-2.16.30-x64-Setup.exe",
                "--output",
                "C:\\users\\IEUser\\Downloads\\WinMerge-2.16.30-x64-Setup.exe",
            ]
        )
        subprocess.call(
            ["C:\\users\\IEUser\\Downloads\\WinMerge-2.16.30-x64-Setup.exe", "/SILENT"]
        )
    except:
        print("WinMerge install failed")
        show_last_exception()
        sys.exit(1)


GRADLE_PATH = "C:\\Users\\IEUser\\Desktop\\Tools\\gradle-8.2\\bin\\"


# https://gradle.org/releases/
def install_gradle():
    if os.path.isdir("C:\\users\\IEUser\\Desktop\\Tools\\gradle-8.2"):
        print("gradle already installed")
        return

    print("Installing gradle...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://services.gradle.org/distributions/gradle-8.2-bin.zip",
                "--output",
                "C:\\users\\IEUser\\Downloads\\gradle-8.2-bin.zip",
            ]
        )
        subprocess.call(
            [
                "C:\\Program Files\\7-Zip\\7z.exe",
                "x",
                "C:\\Users\\IEUser\\Downloads\\gradle-8.2-bin.zip",
                "-oC:\\Users\\IEUser\\Desktop\\Tools\\",
            ]
        )  # zip contains a "gradle-x.x" directory
    except:
        print("gradle install failed")
        show_last_exception()
        sys.exit(1)


def add_gradle_to_path():
    add_to_path("gradle", GRADLE_PATH)


def is_git_in_path():
    git_found = True
    try:
        subprocess.run(["git", "--help"], capture_output=True)
    except FileNotFoundError:
        git_found = False
    return git_found


def is_gradle_in_path():
    found = True
    try:
        ret = subprocess.run(["gradle.bat", "--help"], capture_output=True)
    except FileNotFoundError:
        found = False
    return found


build_retsync_bat = r"""@echo off
echo Setting up build environment...
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64

echo Building...
msbuild "C:\Users\IEUser\Desktop\Tools\ret-sync\ext_windbg\sync\sync\sync.vcxproj" /p:Configuration=Release /p:Platform=x64 /p:WindowsTargetPlatformVersion=%WindowsSDKVersion% /p:PlatformToolset=v142

echo Done
"""

RETSYNC_INSTALL_DIR = "C:\\Users\\IEUser\\Desktop\\tools\\ret-sync"


def install_ret_sync():
    if os.path.isfile(
        "C:\\Users\\IEUSer\\AppData\\Local\\Microsoft\\WindowsApps\\sync.dll"
    ):
        print("ret-sync already installed")
        return

    git_found = is_git_in_path()
    if not git_found:
        # print("git.exe not in PATH. Please manually add \"C:\\Program Files\\Git\\bin\\\" to the PATH. You may have to restart cmd.exe too. Then restart this script.")
        print(
            "Git not installed or not in PATH. You may have to restart cmd.exe too. Then restart this script."
        )
        sys.exit(1)

    print("Installing ret-sync...")
    if not os.path.isdir(RETSYNC_INSTALL_DIR):
        try:
            subprocess.call(
                [
                    "git",
                    "clone",
                    "https://github.com/bootleg/ret-sync",
                    "C:\\Users\\IEUser\\Desktop\\tools\\ret-sync",
                ]
            )
        except:
            print("ret-sync clone failed")
            show_last_exception()
            sys.exit(1)

    # https://www.debugcn.com/en/article/32020717.html
    fd = open("build_retsync.bat", "w")
    fd.write(build_retsync_bat)
    fd.close()

    try:
        subprocess.call(["build_retsync.bat"])
    except:
        print("Build retsync script failed")
        show_last_exception()
        sys.exit(1)

    if not os.path.isfile(
        "C:\\Users\\IEUser\\Desktop\\tools\\ret-sync\\ext_windbg\\sync\\sync\\x64\\Release\\sync.dll"
    ):
        print("Could not build ret-sync")
        sys.exit(1)

    shutil.copy(
        "C:\\Users\\IEUser\\Desktop\\tools\\ret-sync\\ext_windbg\\sync\\sync\\x64\\Release\\sync.dll",
        "C:\\Users\\IEUSer\\AppData\\Local\\Microsoft\\WindowsApps\\sync.dll",
    )


def install_ret_sync2():
    date = datetime.now().strftime("%Y%m%d")
    outname = GHIDRA_VERSION + "_" + date + "_retsync.zip"
    if os.path.isfile(Path(RETSYNC_INSTALL_DIR) / "ext_ghidra" / "dist" / outname):
        print("ret-sync ghidra extension already built")
        return

    found = is_gradle_in_path()
    if not found:
        # print("gradle.bat not in PATH. Please manually add \"C:\\Users\\IEUser\\Desktop\\Tools\\gradle-x.x\\bin\\\" to the PATH. You may have to restart cmd.exe too. Then restart this script.")
        print(
            "gradle not installed or not in PATH. You may have to restart cmd.exe too. Then restart this script."
        )
        sys.exit(1)

    print("Building ret-sync ghidra extension...")
    prev_cwd = Path.cwd()
    os.chdir(Path(RETSYNC_INSTALL_DIR) / "ext_ghidra")
    return_code = None
    try:
        return_code = subprocess.call(
            ["gradle.bat", f"-PGHIDRA_INSTALL_DIR={GHIDRA_INSTALL_DIR}"]
        )
    except:
        print("ret-sync ghidra extension build failed")
        os.chdir(prev_cwd)
        show_last_exception()
        sys.exit(1)

    os.chdir(prev_cwd)
    if return_code != 0:
        print("ret-sync ghidra extension build failed with error = %d" % return_code)
        sys.exit(1)


CMAKE_PATH = "C:\\Program Files\\CMake\\bin\\"


# https://cmake.org/download/
def install_cmake():
    if os.path.isdir("C:\\Program Files\\CMake"):
        print("cmake already installed")
        return

    print("Installing cmake...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "https://github.com/Kitware/CMake/releases/download/v3.26.4/cmake-3.26.4-windows-x86_64.msi",
                "--output",
                "C:\\users\\IEUser\\Downloads\\cmake-3.26.4-windows-x86_64.msi",
            ]
        )
        subprocess.call(
            [
                "msiexec",
                "/i",
                "C:\\users\\IEUser\\Downloads\\cmake-3.26.4-windows-x86_64.msi",
                "/quiet",
                "/qn",
            ]
        )
    except:
        print("cmake install failed")
        show_last_exception()
        sys.exit(1)


def add_cmake_to_path():
    add_to_path("cmake", CMAKE_PATH)


# https://www.catalog.update.microsoft.com/Search.aspx?q=2020-02
# 2020-02 Cumulative Update for Windows 10 Version 1809 for x64-based Systems (KB4537818)
def install_update_2020_02():
    try:
        stdoutdata = subprocess.getoutput("systeminfo | findstr KB4537818")
    except:
        print("2020/02 update detection failed")
        return
    if "KB4537818" in stdoutdata:
        print("2020/02 update already installed")
        return

    print("Installing 2020/02 update, this will take a while...")
    try:
        subprocess.call(
            [
                "curl",
                "-L",
                "http://download.windowsupdate.com/c/msdownload/update/software/updt/2020/02/windows10.0-kb4537818-x64_b8b65276fc7b9694b9c2350c3c3fd4fa60cd682b.msu",
                "--output",
                "C:\\users\\IEUser\\Downloads\\windows10.0-kb4537818-x64_b8b65276fc7b9694b9c2350c3c3fd4fa60cd682b.msu",
            ]
        )
        subprocess.call(
            [
                "wusa",
                "C:\\users\\IEUser\\Downloads\\windows10.0-kb4537818-x64_b8b65276fc7b9694b9c2350c3c3fd4fa60cd682b.msu",
                "/quiet",
                "/norestart",
            ]
        )
    except:
        print("2020/02 update install failed")
        show_last_exception()
        sys.exit(1)
    print(
        "2020/02 update installed, please restart your computer and re-run this script to continue"
    )
    sys.exit(1)


def reboot_system():
    print("[!] Rebooting in 5 seconds...")
    try:
        subprocess.call(["shutdown", "-r", "-t", "5"])
    except:
        print("Rebooting failed")
        sys.exit(1)


def main():
    print("== VM Setup installation script ==")

    if not windll.shell32.IsUserAnAdmin():
        print("This script must be ran as admin!")
        sys.exit(1)

    print("Possible targets:")
    print("1. Target/Debuggee VM")
    print("2. Development/Debugger VM")
    data = input("What VM are you installing? ")
    if data not in ("1", "2"):
        print("Not an appropriate choice.")
        sys.exit()

    if data == "1":
        setup_autoupdate()
        install_7zip()
        install_notepadpp()
        install_firefox()
        install_processhacker()
        install_wsl()
        install_ubuntu()
        setup_windows_defender()
        install_sysinternals()
        setup_taskmanager()
        install_update_2020_02()
    elif data == "2":
        setup_symbols()
        setup_windows_defender()
        install_7zip()
        install_notepadpp()
        install_firefox()
        install_putty()
        install_sysinternals()
        setup_taskmanager()
        install_visualstudio()
        install_git()
        add_git_to_path()
        install_ghidra()
        check_windbg()
        install_gvim()
        install_winmerge()
        install_gradle()
        add_gradle_to_path()
        install_ret_sync()
        install_ret_sync2()
        install_cmake()
        add_cmake_to_path()

    print("All done, you are good to go")


if __name__ == "__main__":
    main()
