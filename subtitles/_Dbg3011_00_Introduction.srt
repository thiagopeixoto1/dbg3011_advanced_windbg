﻿1
00:00:02,000 --> 00:00:06,799
Hi everyone, today we're going to start a

2
00:00:04,480 --> 00:00:10,080
new training which is to have an

3
00:00:06,799 --> 00:00:12,960
advanced WinDbg environment to debug the

4
00:00:10,080 --> 00:00:15,280
kernel on Windows. So, the agenda of this

5
00:00:12,960 --> 00:00:18,000
course is about preparing two virtual

6
00:00:15,280 --> 00:00:20,480
machines, installing a WindDbg kernel

7
00:00:18,000 --> 00:00:22,400
and being able to debug one virtual

8
00:00:20,480 --> 00:00:24,080
machine from the other virtual machine.

9
00:00:22,400 --> 00:00:26,640
We're going to have also to install a

10
00:00:24,080 --> 00:00:28,560
disassembler, either Ghidra or IDA Pro,

11
00:00:26,640 --> 00:00:31,119
and we'll be able to

12
00:00:28,560 --> 00:00:34,000
synchronize the debugger WinDbg with the

13
00:00:31,119 --> 00:00:36,719
actual disassembler using a plugin

14
00:00:34,000 --> 00:00:38,640
called ret-sync, and we'll be able to

15
00:00:36,719 --> 00:00:41,760
have a development environment on one of

16
00:00:38,640 --> 00:00:45,520
the VM to build our binaries and then

17
00:00:41,760 --> 00:00:47,840
push them onto the target VM. So, we will

18
00:00:45,520 --> 00:00:50,000
be able to run the actual binary and

19
00:00:47,840 --> 00:00:52,559
debug it from the debugger VM. So, let's

20
00:00:50,000 --> 00:00:52,559
get started.

