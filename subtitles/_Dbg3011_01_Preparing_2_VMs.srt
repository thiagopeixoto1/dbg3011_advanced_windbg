1
00:00:00,080 --> 00:00:04,480
Hi everyone, in this part the goal is

2
00:00:02,320 --> 00:00:07,200
going to be to build two virtual

3
00:00:04,480 --> 00:00:09,679
machines, one is called the target VM and

4
00:00:07,200 --> 00:00:12,320
the other one is called a debugger VM.

5
00:00:09,679 --> 00:00:15,120
For the target VM, the goal is to be able

6
00:00:12,320 --> 00:00:17,760
to run binaries into it, so we can

7
00:00:15,120 --> 00:00:19,680
analyze what is happening, we can debug

8
00:00:17,760 --> 00:00:22,160
what is happening. For instance we can

9
00:00:19,680 --> 00:00:24,960
analyze the malware, we can analyze an

10
00:00:22,160 --> 00:00:27,039
exploit. For the debugger VM the goal is

11
00:00:24,960 --> 00:00:29,279
to be able to push binaries from the

12
00:00:27,039 --> 00:00:31,920
debugger VM onto the target VM

13
00:00:29,279 --> 00:00:35,680
automatically using SSH. Also, we will be

14
00:00:31,920 --> 00:00:38,239
able to build our own binary from C code

15
00:00:35,680 --> 00:00:40,160
before pushing it onto the target VM. To

16
00:00:38,239 --> 00:00:42,399
build these two virtual machines we're

17
00:00:40,160 --> 00:00:44,719
going to use a Python script to make it

18
00:00:42,399 --> 00:00:46,960
efficient and help you build them

19
00:00:44,719 --> 00:00:49,920
automatically even though some steps are

20
00:00:46,960 --> 00:00:52,239
going to need some manual work. Okay,

21
00:00:49,920 --> 00:00:54,480
let's get started. So, before we actually

2
00:00:52,239 --> 00:00:57,360
install the two virtual machines, I

23
00:00:54,480 --> 00:00:58,559
advise that you actually download the

24
00:00:57,360 --> 00:01:01,920
repository

25
00:00:58,559 --> 00:01:04,320
for this course which is Dbg3011_

26
00:01:01,920 --> 00:01:07,680
Advanced_WinDbg and so the script

27
00:01:04,320 --> 00:01:08,640
that's going to be interested in is setup_vm.py

28
00:01:07,680 --> 00:01:10,880


29
00:01:08,640 --> 00:01:12,960
Because, you're going to have to

30
00:01:10,880 --> 00:01:15,439
install that script on both the debugger

31
00:01:12,960 --> 00:01:17,920
VM and the target VM, but please download

32
00:01:15,439 --> 00:01:20,080
the whole repository, not just that

33
00:01:17,920 --> 00:01:22,159
script because we're going to need the rest

34
00:01:20,080 --> 00:01:25,439
of the material later on in the

35
00:01:22,159 --> 00:01:28,640
training. So, the first thing to say is

36
00:01:25,439 --> 00:01:31,360
that we have our VM built on a template

37
00:01:28,640 --> 00:01:34,400
VM which is going to be Windows 10 1809

38
00:01:31,360 --> 00:01:37,680
and the idea is that we will download

39
00:01:34,400 --> 00:01:39,600
that zip and extract it and then build

40
00:01:37,680 --> 00:01:41,759
our two VMs: the target VM and the

41
00:01:39,600 --> 00:01:44,960
debugger VM. The VM, by default, has a

42
00:01:41,759 --> 00:01:47,280
password, as noted in these slides, so P

43
00:01:44,960 --> 00:01:49,439
capital and ending with an exclamation

44
00:01:47,280 --> 00:01:52,240
point. Let's first start with the target

45
00:01:49,439 --> 00:01:54,880
VM, so we extract the zip and we double

46
00:01:52,240 --> 00:01:58,320
click on the ovf, it's going to ask us to

47
00:01:54,880 --> 00:02:01,119
import the VM as a new VM which we name

48
00:01:58,320 --> 00:02:02,799
Target_VM and we want to have only one

49
00:02:01,119 --> 00:02:04,719
network interface.

50
00:02:02,799 --> 00:02:06,560
We're going to start with the interface

51
00:02:04,719 --> 00:02:08,080
being Host-Only, so it's not connected to

52
00:02:06,560 --> 00:02:09,360
the internet. It's mainly because we want

53
00:02:08,080 --> 00:02:11,760
to avoid these

54
00:02:09,360 --> 00:02:14,560
auto-updates for that particular VM and

55
00:02:11,760 --> 00:02:16,560
will enable internet access after during

56
00:02:14,560 --> 00:02:18,640
installation. We don't need fancy

57
00:02:16,560 --> 00:02:20,800
RAM capabilities, 4 gigabytes is

58
00:02:18,640 --> 00:02:23,760
enough. Generally I like to set up a

59
00:02:20,800 --> 00:02:24,720
number of cores just to support like

60
00:02:23,760 --> 00:02:26,879
testing

61
00:02:24,720 --> 00:02:29,280
multi-threaded applications and stuff like

62
00:02:26,879 --> 00:02:31,840
that. So, before we start installing

63
00:02:29,280 --> 00:02:35,280
stuff on it, this is basically the target

64
00:02:31,840 --> 00:02:38,879
VM. So, we see 4 gigabytes of RAM, two

65
00:02:35,280 --> 00:02:41,760
CPUs each having two cores and we see

66
00:02:38,879 --> 00:02:43,760
that by default it has Host-Only for the

67
00:02:41,760 --> 00:02:47,120
network adapter.

68
00:02:43,760 --> 00:02:49,519
So, we start the target VM.

69
00:02:47,120 --> 00:02:51,680
While is starting, the thing we want to

70
00:02:49,519 --> 00:02:53,599
do is we want to install Python 3, that we

71
00:02:51,680 --> 00:02:55,200
can download from that link and then we

72
00:02:53,599 --> 00:02:58,319
want to

73
00:02:55,200 --> 00:03:00,159
run our setup script into an elevated

74
00:02:58,319 --> 00:03:02,080
prompt and we're going to have to

75
00:03:00,159 --> 00:03:03,599
basically, run that script several times,

76
00:03:02,080 --> 00:03:05,599
each time to

77
00:03:03,599 --> 00:03:07,040
process certain steps. Because we're

78
00:03:05,599 --> 00:03:10,159
installing the target VM, we're going to

79
00:03:07,040 --> 00:03:11,680
actually select our target 1 and just

80
00:03:10,159 --> 00:03:15,120
follow instructions from the setup

81
00:03:11,680 --> 00:03:18,720
script. Okay, so we have our VM now so we

82
00:03:15,120 --> 00:03:21,920
use the password to log in. So here I've

83
00:03:18,720 --> 00:03:23,599
copied Python onto the target VM, so I'm

84
00:03:21,920 --> 00:03:25,120
going to install it now. I'm going to add

85
00:03:23,599 --> 00:03:27,760
it to the PATH,

86
00:03:25,120 --> 00:03:30,080
I'm going to customize installation,

87
00:03:27,760 --> 00:03:33,599
then I install it for all the users in

88
00:03:30,080 --> 00:03:35,840
Program Files and hit install. Okay, so as

89
00:03:33,599 --> 00:03:39,599
we can see Python has been successfully

90
00:03:35,840 --> 00:03:42,319
installed, so you can close that.

91
00:03:39,599 --> 00:03:45,040
The next step is to actually bring our

92
00:03:42,319 --> 00:03:45,040
setup script

93
00:03:45,519 --> 00:03:49,360
onto the VM.

94
00:03:47,200 --> 00:03:51,519
So we're going to start the cmd, I'm

95
00:03:49,360 --> 00:03:53,599
just going to add that to the taskbar

96
00:03:51,519 --> 00:03:55,439
because we're going to use it a lot.

97
00:03:53,599 --> 00:03:58,680
We start a cmd

98
00:03:55,439 --> 00:03:58,680
as Administrator,

99
00:04:00,879 --> 00:04:04,879
and we're going to go into

100
00:04:02,480 --> 00:04:06,239
Users\IEUser\

101
00:04:04,879 --> 00:04:09,120
Desktop

102
00:04:06,239 --> 00:04:11,519
and just start running our setup VM

103
00:04:09,120 --> 00:04:11,519
script.

104
00:04:12,080 --> 00:04:16,879
We select 1, because we're building our

105
00:04:14,159 --> 00:04:16,879
target VM.

106
00:04:17,519 --> 00:04:22,240
So, now we can see that it is telling us

107
00:04:20,239 --> 00:04:24,320
to change the network interface to NAT,

108
00:04:22,240 --> 00:04:28,400
because we've successfully disabled the

109
00:04:24,320 --> 00:04:31,840
auto update. So, we do Ctrl+D and just

110
00:04:28,400 --> 00:04:31,840
change Host-Only to NAT.

111
00:04:34,720 --> 00:04:40,840
So, now we can see

112
00:04:36,880 --> 00:04:40,840
we don't have a connection yet.

113
00:04:42,160 --> 00:04:45,759
Okay, so now we're connected. So now, we

114
00:04:44,240 --> 00:04:48,960
can rerun

115
00:04:45,759 --> 00:04:51,360
our setup script and process additional

116
00:04:48,960 --> 00:04:54,400
installations.

117
00:04:51,360 --> 00:04:54,400
Again, we select 1.

118
00:04:54,800 --> 00:04:58,479
So, while it's installing, we have a

119
00:04:56,560 --> 00:05:01,039
couple of things we can do. We want to

120
00:04:58,479 --> 00:05:02,960
configure the actual language, if you

121
00:05:01,039 --> 00:05:05,440
have a different keyboard than the US default

122
00:05:02,960 --> 00:05:07,680
one. You want to be able to disable auto

123
00:05:05,440 --> 00:05:09,440
shutdown of the virtual machine so it's

124
00:05:07,680 --> 00:05:11,600
easier to deal with it. And then, we want

125
00:05:09,440 --> 00:05:14,160
to disable the password requirement at

126
00:05:11,600 --> 00:05:16,639
boot. So, to do that

127
00:05:14,160 --> 00:05:19,120
we're going to Language,

128
00:05:16,639 --> 00:05:19,120
Settings,

129
00:05:20,240 --> 00:05:24,840
then here we select our new language.

130
00:05:24,960 --> 00:05:29,360
So, in my case I have a UK keyboard

131
00:05:34,800 --> 00:05:40,479
and now I can select my new keyboard.

132
00:05:38,720 --> 00:05:42,960
The next thing is to disable auto

133
00:05:40,479 --> 00:05:42,960
shutdown,

134
00:05:43,039 --> 00:05:48,800
Power & sleep settings,

135
00:05:45,360 --> 00:05:48,800
and just change that to never,

136
00:05:49,039 --> 00:05:52,400
both of them. And the last thing is to go

137
00:05:51,120 --> 00:05:53,360
into

138
00:05:52,400 --> 00:05:56,160
net

139
00:05:53,360 --> 00:05:56,160
plwiz

140
00:05:57,440 --> 00:06:02,000
and then just untick the Users must

141
00:06:00,240 --> 00:06:03,440
enter a username and password to use

142
00:06:02,000 --> 00:06:06,720
this computer.

143
00:06:03,440 --> 00:06:09,360
You're going to have to use the password

144
00:06:06,720 --> 00:06:09,360
here twice

145
00:06:12,639 --> 00:06:18,400
and click OK. Okay, so now we can see that

146
00:06:15,680 --> 00:06:20,560
the script has successfully installed

147
00:06:18,400 --> 00:06:23,520
the Windows Linux subsystem. It is asking us

148
00:06:20,560 --> 00:06:26,000
to restart and rerun the script. So, let's

149
00:06:23,520 --> 00:06:26,000
restart.

150
00:06:29,199 --> 00:06:32,840
Okay, so now we have restarted our

151
00:06:30,639 --> 00:06:35,919
the virtual machine, we're going to continue

152
00:06:32,840 --> 00:06:38,639
installing software we run our cmd with

153
00:06:35,919 --> 00:06:42,160
Administrator privileges.

154
00:06:38,639 --> 00:06:45,840
We go into Users\IEUser\Desktop

155
00:06:42,160 --> 00:06:45,840
and continue installing it.

156
00:06:47,199 --> 00:06:50,400
Again we select 1.

157
00:06:52,800 --> 00:06:58,080
So while it's actually installing Ubuntu,

158
00:06:55,280 --> 00:07:00,319
we can see that once it is installed,

159
00:06:58,080 --> 00:07:03,039
what we'll have to do is basically to

160
00:07:00,319 --> 00:07:06,080
run the ubuntu command and then create a

161
00:07:03,039 --> 00:07:08,800
username named "user" and then the password

162
00:07:06,080 --> 00:07:12,639
set to "user" as well. And then what we're

163
00:07:08,800 --> 00:07:14,880
going to have to set up is SSH, into this

164
00:07:12,639 --> 00:07:17,919
Ubuntu environment. Basically we'll have

165
00:07:14,880 --> 00:07:20,000
to generate keys for that SSH server

166
00:07:17,919 --> 00:07:22,160
running on that target VM, so it is

167
00:07:20,000 --> 00:07:25,280
working properly. And then we'll add

168
00:07:22,160 --> 00:07:28,319
trusted keys to the SSH server, so it

169
00:07:25,280 --> 00:07:30,880
allows a hard coded key that will be

170
00:07:28,319 --> 00:07:34,319
used by the debugger VM. So we are

171
00:07:30,880 --> 00:07:37,199
providing a key pair in the ssh

172
00:07:34,319 --> 00:07:40,639
-config folder. The private and public

173
00:07:37,199 --> 00:07:43,120
keys are labeled target_vm and they are

174
00:07:40,639 --> 00:07:45,840
keys used by the debugger VM to

175
00:07:43,120 --> 00:07:49,759
authenticate to the target VM. The reason

176
00:07:45,840 --> 00:07:52,240
why the PPK and the PUB files are named

177
00:07:49,759 --> 00:07:54,560
target_vm, is because a lot of people use

178
00:07:52,240 --> 00:07:57,360
a different public and private key pair

179
00:07:54,560 --> 00:08:00,000
for each host that they authenticate to,

180
00:07:57,360 --> 00:08:02,560
in which case the keys are named after

181
00:08:00,000 --> 00:08:04,720
whatever the remote system is. So it can

182
00:08:02,560 --> 00:08:06,879
be confusing at first but this is how

183
00:08:04,720 --> 00:08:10,319
people generally do it on all of their

184
00:08:06,879 --> 00:08:11,599
systems. So, to summarize the keys in the

185
00:08:10,319 --> 00:08:14,080
ssh

186
00:08:11,599 --> 00:08:16,720
-config folder are keys used to

187
00:08:14,080 --> 00:08:21,680
authenticate from the debugger VM into

188
00:08:16,720 --> 00:08:25,039
the target VM. We use sudo ssh-keygen

189
00:08:21,680 --> 00:08:27,039
-A to generate the SSH server keys on the

190
00:08:25,039 --> 00:08:29,599
target VM, which are different from the

191
00:08:27,039 --> 00:08:31,440
public and private key pair authorized

192
00:08:29,599 --> 00:08:34,000
to authenticate to that server. The

193
00:08:31,440 --> 00:08:36,000
public/private key pair used to

194
00:08:34,000 --> 00:08:38,080
authenticate are the keys used by any

195
00:08:36,000 --> 00:08:40,159
user in order to authenticate when

196
00:08:38,080 --> 00:08:42,320
connecting to the target VM. So, here we

197
00:08:40,159 --> 00:08:45,200
are authorizing a specific keypair in

198
00:08:42,320 --> 00:08:47,760
the authorized_key file to allow the

199
00:08:45,200 --> 00:08:50,320
future debugger VM to connect to it. And

200
00:08:47,760 --> 00:08:52,880
we do that by pasting a hard coded

201
00:08:50,320 --> 00:08:54,399
the public key from the ssh-config folder.

202
00:08:52,880 --> 00:08:56,560
And finally, we are setting the right

203
00:08:54,399 --> 00:08:59,120
permission for the folders and files to

204
00:08:56,560 --> 00:09:01,120
allow the SSH server to work properly

205
00:08:59,120 --> 00:09:03,680
and securely. And then we are going to be

206
00:09:01,120 --> 00:09:05,440
able to start the SSH service and then

207
00:09:03,680 --> 00:09:07,360
allow the service to go over the

208
00:09:05,440 --> 00:09:09,839
firewall. Also please note that you will

209
00:09:07,360 --> 00:09:12,320
have to actually restart the SSH service

210
00:09:09,839 --> 00:09:14,640
each time you reboot the target VM

211
00:09:12,320 --> 00:09:16,720
because it won't be done automatically.

212
00:09:14,640 --> 00:09:19,600
So, we can see that Ubuntu is being

213
00:09:16,720 --> 00:09:22,320
installed and now it's telling us to run

214
00:09:19,600 --> 00:09:26,880
the ubuntu command and to create our own

215
00:09:22,320 --> 00:09:26,880
user. So, let's start our ubuntu.

216
00:09:28,480 --> 00:09:32,240
Again, it's going to take a few minutes

217
00:09:30,160 --> 00:09:35,440
to install Ubuntu. Okay, so now it's

218
00:09:32,240 --> 00:09:37,120
asking to create the user. So, we're

219
00:09:35,440 --> 00:09:38,240
going to create the user

220
00:09:37,120 --> 00:09:39,279
"user"

221
00:09:38,240 --> 00:09:41,040
and

222
00:09:39,279 --> 00:09:43,440
for the password we're going to also use

223
00:09:41,040 --> 00:09:43,440
"user".

224
00:09:49,519 --> 00:09:56,000
So, now we're going to set up SSH,

225
00:09:51,680 --> 00:09:58,080
so we're going to use sudo ssh-keygen -A,

226
00:09:56,000 --> 00:10:00,720
our password is "user",

227
00:09:58,080 --> 00:10:02,800
then we are creating the .ssh folder,

228
00:10:00,720 --> 00:10:05,839
and we are going to modify our

229
00:10:02,800 --> 00:10:08,320
authorized_keys. So, here you can paste

230
00:10:05,839 --> 00:10:10,720
the content of the target_vm_pub_authorized

231
00:10:08,320 --> 00:10:10,720
file.

232
00:10:13,040 --> 00:10:16,560
We use a hard-coded SSH key because

233
00:10:15,120 --> 00:10:18,240
that's the one we're going to use into

234
00:10:16,560 --> 00:10:21,360
the Visual Studio environment on

235
00:10:18,240 --> 00:10:23,200
the debugger VM side.

236
00:10:21,360 --> 00:10:26,399
Then, we change the permission of the .ssh

237
00:10:23,200 --> 00:10:27,519
folder, and finally of the authorized

238
00:10:26,399 --> 00:10:30,800
_keys.

239
00:10:27,519 --> 00:10:33,800
And now we can start our SSH

240
00:10:30,800 --> 00:10:33,800
service.

241
00:10:34,079 --> 00:10:40,480
Here, we allow the access

242
00:10:36,880 --> 00:10:43,839
over the firewall for the SSH server.

243
00:10:40,480 --> 00:10:46,640
So, now we can exit the Windows Subsystem,

244
00:10:43,839 --> 00:10:48,560
and again we're going to rerun the setup

245
00:10:46,640 --> 00:10:51,279
script to see if there is anything else

246
00:10:48,560 --> 00:10:51,279
we need to install.

247
00:10:51,680 --> 00:10:56,720
Again, we set up the target VM.

248
00:10:54,800 --> 00:10:57,440
So, as you can see now,

249
00:10:56,720 --> 00:10:59,680
the

250
00:10:57,440 --> 00:11:01,279
update has been installed and it asks us

251
00:10:59,680 --> 00:11:02,399
to restart the computer and rerun the

252
00:11:01,279 --> 00:11:05,040
script.

253
00:11:02,399 --> 00:11:07,600
So, after the VM has restarted you may be

254
00:11:05,040 --> 00:11:10,480
asked about the password again,

255
00:11:07,600 --> 00:11:12,839
so if that's the case you may have to

256
00:11:10,480 --> 00:11:17,190
again disable the password requirement at

257
00:11:12,839 --> 00:11:20,320
boot. So going into netplwiz

258
00:11:17,190 --> 00:11:22,000


259
00:11:20,320 --> 00:11:25,000
and then unticking

260
00:11:22,000 --> 00:11:25,000
this.

261
00:11:30,000 --> 00:11:35,120
Okay, so we're going to rerun the

262
00:11:32,240 --> 00:11:35,120
installer script

263
00:11:35,600 --> 00:11:41,600
in a cmd,

264
00:11:38,640 --> 00:11:44,320
going into Users\IEUser\Desktop and we just

265
00:11:41,600 --> 00:11:44,320
run setup.

266
00:11:53,200 --> 00:11:55,760
Okay,

267
00:11:54,160 --> 00:11:58,959
so now we can see that everything has

268
00:11:55,760 --> 00:12:00,880
been installed and we are good to go. So

269
00:11:58,959 --> 00:12:03,519
the last step we want to do is we want

270
00:12:00,880 --> 00:12:06,880
to change the network interface of our

271
00:12:03,519 --> 00:12:09,360
virtual machine with Ctrl+D.

272
00:12:06,880 --> 00:12:11,839
We can change the network adapter to

273
00:12:09,360 --> 00:12:11,839
Host-Only.

274
00:12:12,639 --> 00:12:17,519
And finally, we can shut down the virtual

275
00:12:15,120 --> 00:12:17,519
machine.

276
00:12:20,959 --> 00:12:24,720
So the last thing we want to do is

277
00:12:23,040 --> 00:12:26,720
do a snapshot of the virtual machine so

278
00:12:24,720 --> 00:12:28,560
we do Ctrl+M

279
00:12:26,720 --> 00:12:31,760
and then we

280
00:12:28,560 --> 00:12:31,760
say "installation".

281
00:12:36,480 --> 00:12:41,360
So, now our target VM is installed. So now

282
00:12:39,040 --> 00:12:43,760
that our target VM is configured, we're

283
00:12:41,360 --> 00:12:46,000
going to do the same for the debugger VM. So

284
00:12:43,760 --> 00:12:49,120
again we're going to extract our zip

285
00:12:46,000 --> 00:12:52,399
and double click on the ovf to

286
00:12:49,120 --> 00:12:54,079
import a second VM which we're going to

287
00:12:52,399 --> 00:12:55,440
name Debugger_VM.

288
00:12:54,079 --> 00:12:58,000
In this VM, we're gonna set up two

289
00:12:55,440 --> 00:13:00,079
network interfaces, one to connect to the

290
00:12:58,000 --> 00:13:02,000
internet called NAT interface and then

291
00:13:00,079 --> 00:13:05,120
the other one Host-Only interface so

292
00:13:02,000 --> 00:13:06,959
it can interact with the target VM. And

293
00:13:05,120 --> 00:13:08,560
for this VM we're going to set up more

294
00:13:06,959 --> 00:13:10,079
RAM because we're going to install

295
00:13:08,560 --> 00:13:13,920
different software like the debugger

296
00:13:10,079 --> 00:13:15,040
disassembler. So, 8 gigabytes is nice.

297
00:13:13,920 --> 00:13:17,040
So, the first thing we want to do is

298
00:13:15,040 --> 00:13:18,560
again is we're going to set up the

299
00:13:17,040 --> 00:13:20,959
keyboard,

300
00:13:18,560 --> 00:13:23,120
disable auto shutdown and disable the

301
00:13:20,959 --> 00:13:25,360
password requirement at boot. So, if we

302
00:13:23,120 --> 00:13:26,880
access our debugger VM, we see 8 gigs

303
00:13:25,360 --> 00:13:28,480
of RAM, you see the two network

304
00:13:26,880 --> 00:13:31,600
interfaces

305
00:13:28,480 --> 00:13:31,600
and let's boot it.

306
00:13:32,399 --> 00:13:40,839
Okay, so now we can configure the

307
00:13:36,240 --> 00:13:40,839
keyboard similary to the other VM,

308
00:13:48,959 --> 00:13:53,480
we can disable auto shutdown.

309
00:14:03,600 --> 00:14:07,680
I'm going to select my keyboard

310
00:14:05,760 --> 00:14:11,639
and I'm going to disable the password

311
00:14:07,680 --> 00:14:11,639
requirements at boot.

312
00:14:22,320 --> 00:14:26,639
The next step is to install Python, so

313
00:14:24,639 --> 00:14:28,639
here I copied Python

314
00:14:26,639 --> 00:14:30,399
installing it now,

315
00:14:28,639 --> 00:14:32,720
adding it to the PATH,

316
00:14:30,399 --> 00:14:34,639
customizing installation,

317
00:14:32,720 --> 00:14:37,040
installing for all users in Program

318
00:14:34,639 --> 00:14:37,040
Files.

319
00:14:41,440 --> 00:14:45,519
So, we also want to install Java and the

320
00:14:44,240 --> 00:14:47,839
reason for that is because we're going

321
00:14:45,519 --> 00:14:49,600
to use Ghidra and so to install Java you

322
00:14:47,839 --> 00:14:51,680
need to download it from the Oracle

323
00:14:49,600 --> 00:14:54,079
website you need to create an Oracle

324
00:14:51,680 --> 00:14:59,000
account, but it's free to do so and you

325
00:14:54,079 --> 00:14:59,000
end up having a JDK executable.

326
00:15:00,320 --> 00:15:03,600
I'm gonna install the JDK,

327
00:15:10,959 --> 00:15:14,720
just use the default installation.

328
00:15:18,079 --> 00:15:22,639
So, now we can confirm we have both Python

329
00:15:21,190 --> 00:15:25,959


330
00:15:22,639 --> 00:15:25,959
and Java.

331
00:15:26,399 --> 00:15:30,240
The next thing we want to install is

332
00:15:28,240 --> 00:15:33,199
WinDbg Preview.

333
00:15:30,240 --> 00:15:35,440
So, you can either find it by

334
00:15:33,199 --> 00:15:39,480
browsing on the browser or just

335
00:15:35,440 --> 00:15:39,480
searching on the Microsoft Store.

336
00:15:44,480 --> 00:15:47,440
Just do get.

337
00:15:49,040 --> 00:15:52,160
It asks you if you want to sign in, you

338
00:15:50,560 --> 00:15:53,920
can just say no.

339
00:15:52,160 --> 00:15:57,199
WinDbg Preview is installed, so now we

340
00:15:53,920 --> 00:15:57,199
can launch this application.

341
00:15:59,040 --> 00:16:02,160
And we can even pin it to the taskbar

342
00:16:01,120 --> 00:16:04,240
for later.

343
00:16:02,160 --> 00:16:06,800
So, the next thing is we're going to run

344
00:16:04,240 --> 00:16:09,199
a setup script again similar to what

345
00:16:06,800 --> 00:16:12,880
we did it on the actual target VM and we're

346
00:16:09,199 --> 00:16:12,880
going to have to install a few things.

347
00:16:13,759 --> 00:16:19,440
So, we run our

348
00:16:16,240 --> 00:16:19,440
Administrator cmd

349
00:16:20,079 --> 00:16:22,800
then we're going into

350
00:16:21,440 --> 00:16:25,360
Users\

351
00:16:22,800 --> 00:16:27,279
IEUser\Desktop

352
00:16:25,360 --> 00:16:29,680
and then run it.

353
00:16:27,279 --> 00:16:34,320
So, in this case obviously we're going to

354
00:16:29,680 --> 00:16:34,320
use the target 2 for the debugger VM.

355
00:16:34,959 --> 00:16:39,519
So, we can see while trying to install

356
00:16:36,880 --> 00:16:41,199
Ghidra, it's actually telling us that it

357
00:16:39,519 --> 00:16:42,639
didn't find git because it's not in the

358
00:16:41,199 --> 00:16:44,480
PATH. So,

359
00:16:42,639 --> 00:16:45,759
even though it's not in the PATH yet it,

360
00:16:44,480 --> 00:16:48,320
was actually installed in the background,

361
00:16:45,759 --> 00:16:51,839
so if you just exit cmd

362
00:16:48,320 --> 00:16:51,839
and just rerun it,

363
00:16:53,680 --> 00:16:57,120
again we're going to C:\Users,

364
00:16:57,839 --> 00:17:00,639
rerun the script

365
00:17:03,279 --> 00:17:08,959
this time it's finding git and it's

366
00:17:05,439 --> 00:17:08,959
continuing to install ret-sync.

367
00:17:09,520 --> 00:17:13,520
So, now we can see the script said all

368
00:17:11,679 --> 00:17:15,520
done you're good to go, just as a safety

369
00:17:13,520 --> 00:17:16,799
measure, we're just going to rerun it one

370
00:17:15,520 --> 00:17:20,240
last time

371
00:17:16,799 --> 00:17:20,240
to make sure everything was installed,

372
00:17:20,559 --> 00:17:24,000
and you can see everything has been

373
00:17:22,240 --> 00:17:26,240
installed successfully and you are good

374
00:17:24,000 --> 00:17:30,160
to go. So, the last thing we want to do is

375
00:17:26,240 --> 00:17:33,039
want to copy the Tools folder.

376
00:17:30,160 --> 00:17:34,799
Inside the Tools folder, we see that

377
00:17:33,039 --> 00:17:36,240
Ghidra, ret-sync and SysInternals were

378
00:17:34,799 --> 00:17:38,240
installed.

379
00:17:36,240 --> 00:17:39,679
So, we're just going to copy all

380
00:17:38,240 --> 00:17:42,720
the actual files

381
00:17:39,679 --> 00:17:42,720
that we'll need later.

382
00:17:45,520 --> 00:17:49,919
And the last step is to basically shut

383
00:17:47,440 --> 00:17:52,000
down the VM,

384
00:17:49,919 --> 00:17:53,520
and do a snapshot of it. So, to do a

385
00:17:52,000 --> 00:17:55,760
snapshot of it, you

386
00:17:53,520 --> 00:17:57,280
do Ctrl+M

387
00:17:55,760 --> 00:18:00,280
and then

388
00:17:57,280 --> 00:18:00,280
installation.

389
00:18:01,679 --> 00:18:07,120
Okay, we have our target VM and debugger

390
00:18:04,320 --> 00:18:10,160
VM configured. Let's do the next software

391
00:18:07,120 --> 00:18:10,160
installation. Thank you!

