1
00:00:00,560 --> 00:00:04,560
Hey everyone, in this part we're going to

2
00:00:02,080 --> 00:00:08,080
show you how to configure Visual Studio

3
00:00:04,560 --> 00:00:10,240
on your debugger VM and then push your

4
00:00:08,080 --> 00:00:11,759
built binary onto the target VM

5
00:00:10,240 --> 00:00:14,960
automatically. For that, we're going to

6
00:00:11,759 --> 00:00:17,199
use SSH configured on the target VM. Okay,

7
00:00:14,960 --> 00:00:20,480
let's get started. So we provide a

8
00:00:17,199 --> 00:00:23,279
visual_studio_labs.zip archive that you can extract

9
00:00:20,480 --> 00:00:25,359
to get basically the labs to generate

10
00:00:23,279 --> 00:00:27,119
the Visual Studio solution. And the idea

11
00:00:25,359 --> 00:00:29,599
is we're not going to provide the actual

12
00:00:27,119 --> 00:00:32,160
solution and projects already. We use

13
00:00:29,599 --> 00:00:34,559
cmake to generate them. So, the first time

14
00:00:32,160 --> 00:00:36,480
you're going to actually build the solution,

15
00:00:34,559 --> 00:00:38,079
you're going to actually run a build.bat

16
00:00:36,480 --> 00:00:39,440
script and this will generate the

17
00:00:38,079 --> 00:00:41,120
solution and the project for you. The

18
00:00:39,440 --> 00:00:44,000
first time you do it, you can use

19
00:00:41,120 --> 00:00:46,559
"build.bat hello" for quick testing. After

20
00:00:44,000 --> 00:00:48,800
that you're going to have a build\ directory

21
00:00:46,559 --> 00:00:51,039
generated with a solution into that. So for

22
00:00:48,800 --> 00:00:53,120
all the future generations or all the

23
00:00:51,039 --> 00:00:55,280
future builds of your binary, you can just

24
00:00:53,120 --> 00:00:57,520
use the solution and the general Visual

25
00:00:55,280 --> 00:01:00,719
Studio method to build the project. So, on

26
00:00:57,520 --> 00:01:02,800
the target VM side, we want to make sure

27
00:01:00,719 --> 00:01:04,879
SSH is enabled. We should have configured

28
00:01:02,800 --> 00:01:07,760
it but if you rebooted your VM you may

29
00:01:04,879 --> 00:01:09,840
have to rerun the SSH service. This is

30
00:01:07,760 --> 00:01:12,880
because Visual Studio will be configured

31
00:01:09,840 --> 00:01:15,439
to automatically push the built binary

32
00:01:12,880 --> 00:01:18,159
onto the target VM over SSH. So, to do

33
00:01:15,439 --> 00:01:21,520
that we invoke an external batch script

34
00:01:18,159 --> 00:01:23,360
called scp.bat and this requires some

35
00:01:21,520 --> 00:01:25,920
environment variables like the IP

36
00:01:23,360 --> 00:01:28,799
address where to push the binary, which

37
00:01:25,920 --> 00:01:32,240
will be the target VM IP, and the user

38
00:01:28,799 --> 00:01:34,960
to connect to the Windows subsystem SSH.

39
00:01:32,240 --> 00:01:37,040
It also requires a PuTTY profile on that

40
00:01:34,960 --> 00:01:39,920
debugger VM that needs to be configured

41
00:01:37,040 --> 00:01:43,360
and that needs to have the IP address, as

42
00:01:39,920 --> 00:01:46,000
well as the information like the SSH

43
00:01:43,360 --> 00:01:47,680
certificate to connect to the target VM.

44
00:01:46,000 --> 00:01:49,200
We'll show you how to configure that in

45
00:01:47,680 --> 00:01:51,360
a second. You want to make sure as well,

46
00:01:49,200 --> 00:01:54,079
that the actual target VM is not on a

47
00:01:51,360 --> 00:01:56,399
breakpoint, stuck into a debugger or on a

48
00:01:54,079 --> 00:01:58,880
getchar(), for the actual binary you're

49
00:01:56,399 --> 00:02:00,719
trying to push, to make sure you are able

50
00:01:58,880 --> 00:02:02,640
to actually write that file on the

51
00:02:00,719 --> 00:02:04,240
target VM. This is the Visual Studio

52
00:02:02,640 --> 00:02:06,399
environment, where you can see on the

53
00:02:04,240 --> 00:02:08,080
right the different parts, the part0 and

54
00:02:06,399 --> 00:02:11,120
the helloworld folder, and then we have

55
00:02:08,080 --> 00:02:12,959
the HelloWorld_lab, and the HelloWorld.c

56
00:02:11,120 --> 00:02:15,520
file. On the left side, you can see that

57
00:02:12,959 --> 00:02:17,680
there is the actual lab instructions,

58
00:02:15,520 --> 00:02:19,200
if you look for LAB TODO you will

59
00:02:17,680 --> 00:02:20,879
see the different things you have to do

60
00:02:19,200 --> 00:02:23,760
for the lab. At the bottom you see that

61
00:02:20,879 --> 00:02:25,640
we have actually built the project and

62
00:02:23,760 --> 00:02:28,239
what happened in the

63
00:02:25,640 --> 00:02:30,319
HelloWorld_lab.exe was actually compiled

64
00:02:28,239 --> 00:02:32,720
and after that, you see the SCP

65
00:02:30,319 --> 00:02:36,239
invocation that actually pushed the file

66
00:02:32,720 --> 00:02:38,400
onto the remote target VM. So, this is the

67
00:02:36,239 --> 00:02:40,080
script that is invoked the first time to

68
00:02:38,400 --> 00:02:42,800
set up the environment variables so we

69
00:02:40,080 --> 00:02:45,280
can see the remote IP for the target VM,

70
00:02:42,800 --> 00:02:48,160
we see the user for SSH and the remote

71
00:02:45,280 --> 00:02:50,239
path in the WSL context to actually

72
00:02:48,160 --> 00:02:52,640
write that file on the Desktop. In order

73
00:02:50,239 --> 00:02:55,120
for the copy of our SSH to work it's

74
00:02:52,640 --> 00:02:57,599
going to rely on a private key being

75
00:02:55,120 --> 00:02:59,680
configured directly on the debugger VM,

76
00:02:57,599 --> 00:03:01,920
so you don't have to input the password

77
00:02:59,680 --> 00:03:04,000
each time you build and push a binary

78
00:03:01,920 --> 00:03:06,080
onto the target VM. So, for that to work

79
00:03:04,000 --> 00:03:08,080
we rely on a PuTTY profile, we need to

80
00:03:06,080 --> 00:03:09,840
create a specific session on that

81
00:03:08,080 --> 00:03:11,680
profile with the IP address matching the

82
00:03:09,840 --> 00:03:13,040
target VM for the actual name of the

83
00:03:11,680 --> 00:03:15,519
session. And...

84
00:03:13,040 --> 00:03:17,920
we'll show you later but we need also to

85
00:03:15,519 --> 00:03:20,800
provide the actual private key as well

86
00:03:17,920 --> 00:03:22,319
as the user for that SSH session. So, this

87
00:03:20,800 --> 00:03:24,400
is basically the three things you need

88
00:03:22,319 --> 00:03:25,920
to do. On the left we see the host name

89
00:03:24,400 --> 00:03:28,159
which is the IP address we connect to,

90
00:03:25,920 --> 00:03:30,480
and the port 22, but also the saved

91
00:03:28,159 --> 00:03:32,319
session for that needs to match the IP

92
00:03:30,480 --> 00:03:34,159
address for the name for that to work.

93
00:03:32,319 --> 00:03:36,480
In the middle, you see the auto-login

94
00:03:34,159 --> 00:03:39,360
username is set to "user" and on the right,

95
00:03:36,480 --> 00:03:40,799
we see in the SSH > Auth, we configured the

96
00:03:39,360 --> 00:03:42,640
path to the private key for

97
00:03:40,799 --> 00:03:45,120
authentication. So, the way to test that

98
00:03:42,640 --> 00:03:47,280
you have your environment working is to

99
00:03:45,120 --> 00:03:48,799
build the project on the debugger VM,

100
00:03:47,280 --> 00:03:50,959
using the build script and it should

101
00:03:48,799 --> 00:03:52,799
build the binary automatically and push

102
00:03:50,959 --> 00:03:54,879
it onto the target VM desktop. Then you

103
00:03:52,799 --> 00:03:56,720
can go to your target VM and actually

104
00:03:54,879 --> 00:03:58,799
run the binary you've just pushed and

105
00:03:56,720 --> 00:04:00,400
built. Okay, so we have our target VM and

106
00:03:58,799 --> 00:04:02,239
our debugger VM.

107
00:04:00,400 --> 00:04:04,959
So on the debugger VM, you should have

108
00:04:02,239 --> 00:04:06,159
pushed the visual_studio_labs.zip

109
00:04:04,959 --> 00:04:07,439
archive

110
00:04:06,159 --> 00:04:09,599
that we're going to

111
00:04:07,439 --> 00:04:11,519
extract. You can see it has an actual

112
00:04:09,599 --> 00:04:12,319
folder in it. So, we're just gonna extract

113
00:04:11,519 --> 00:04:15,319
it

114
00:04:12,319 --> 00:04:15,319
here.

115
00:04:16,160 --> 00:04:21,440
And in that directory, you'll see

116
00:04:18,799 --> 00:04:24,479
a build\ folder that is empty

117
00:04:21,440 --> 00:04:28,120
and the actual files with the build.bat

118
00:04:24,479 --> 00:04:28,120
that's going to use cmake.

119
00:04:28,240 --> 00:04:32,320
So, we're going to

120
00:04:29,680 --> 00:04:34,160
Desktop\Tools\

121
00:04:32,320 --> 00:04:36,400
visual_studio_labs\ and now we're going

122
00:04:34,160 --> 00:04:39,400
to build, we are going to invoke build.bat with

123
00:04:36,400 --> 00:04:39,400
hello.

124
00:04:48,639 --> 00:04:52,560
So, as you can see, what happened is that

125
00:04:51,199 --> 00:04:56,960
it tried to set the environment

126
00:04:52,560 --> 00:04:58,000
variables using the env.bat script.

127
00:04:56,960 --> 00:04:59,759
And...

128
00:04:58,000 --> 00:05:03,120
then it built the actual

129
00:04:59,759 --> 00:05:03,120
hello project.

130
00:05:06,000 --> 00:05:10,080
And then after it created the hello

131
00:05:07,840 --> 00:05:12,320
world project, it actually tried to push

132
00:05:10,080 --> 00:05:14,080
the file onto that IP address. So the IP

133
00:05:12,320 --> 00:05:15,919
address in your case may be different, so

134
00:05:14,080 --> 00:05:17,680
it's going to fail, and also the problem

135
00:05:15,919 --> 00:05:20,479
is we don't have any PuTTY profile at

136
00:05:17,680 --> 00:05:22,479
the moment so it's not able to do so.

137
00:05:20,479 --> 00:05:24,240
So, we're just going to close that, so now

138
00:05:22,479 --> 00:05:26,240
you should have inside the build\

139
00:05:24,240 --> 00:05:27,840
directory the actual solution that you

140
00:05:26,240 --> 00:05:31,039
can start and we can build everything

141
00:05:27,840 --> 00:05:31,039
from that solution now.

142
00:05:33,600 --> 00:05:37,039
So, you can see

143
00:05:35,440 --> 00:05:39,600
the different parts for the different

144
00:05:37,039 --> 00:05:42,479
labs in this case we only have one lab

145
00:05:39,600 --> 00:05:42,479
HelloWorld.c

146
00:05:42,960 --> 00:05:49,440
and so we know that this will invoke

147
00:05:46,639 --> 00:05:50,479
two scripts, so in visual_studio_labs\

148
00:05:49,440 --> 00:05:54,880
scripts\,

149
00:05:50,479 --> 00:05:54,880
we're going to have env.bat and scp.bat.

150
00:05:57,520 --> 00:06:02,319
So env.bat will be

151
00:06:00,240 --> 00:06:04,560
setting the environment variables so you

152
00:06:02,319 --> 00:06:07,560
need that IP address to match the target

153
00:06:04,560 --> 00:06:07,560
VM.

154
00:06:08,319 --> 00:06:13,319
So, here we have the target VM IP 92.131

155
00:06:13,840 --> 00:06:18,479
and this needs to match here to be able

156
00:06:16,319 --> 00:06:21,280
to push the binary from the debugger VM

157
00:06:18,479 --> 00:06:24,479
to the target VM, this needs to be here

158
00:06:21,280 --> 00:06:27,039
as well. So scp.bat will

159
00:06:24,479 --> 00:06:29,840
assume env.bat has been executed and will

160
00:06:27,039 --> 00:06:32,560
rely on the actual environment variables

161
00:06:29,840 --> 00:06:35,840
and then it will use PuTTY

162
00:06:32,560 --> 00:06:36,960
to scp, to copy the built binaries over

163
00:06:35,840 --> 00:06:39,440
SSH.

164
00:06:36,960 --> 00:06:42,080
So, once we have done that, we need to

165
00:06:39,440 --> 00:06:42,080
start PuTTY,

166
00:06:44,720 --> 00:06:49,680
and we need to define

167
00:06:47,039 --> 00:06:51,919
an actual SSH profile

168
00:06:49,680 --> 00:06:55,360
so we need the host name to be the IP

169
00:06:51,919 --> 00:06:58,080
address then we need to go into data and

170
00:06:55,360 --> 00:07:00,639
set the user to "user"

171
00:06:58,080 --> 00:07:02,080
and then in SSH > Auth,

172
00:07:00,639 --> 00:07:04,800
we need to select the private key for

173
00:07:02,080 --> 00:07:04,800
authentication.

174
00:07:06,960 --> 00:07:12,599
It should be into ssh-config

175
00:07:09,599 --> 00:07:12,599
target_vm.ppk.

176
00:07:13,120 --> 00:07:18,319
Once you've done that, you go back to

177
00:07:14,840 --> 00:07:20,240
session and you should save the session

178
00:07:18,319 --> 00:07:24,960
with the same name

179
00:07:20,240 --> 00:07:27,039
at the actual IP address and click save.

180
00:07:24,960 --> 00:07:31,639
Now we try to open a

181
00:07:27,039 --> 00:07:31,639
connection just to confirm it works.

182
00:07:33,199 --> 00:07:36,800
So, as you can see it doesn't work

183
00:07:38,319 --> 00:07:42,599
so if we go back to the target VM

184
00:07:45,759 --> 00:07:51,360
when we do sudo

185
00:07:48,720 --> 00:07:53,759
service ssh

186
00:07:51,360 --> 00:07:53,759
start,

187
00:07:56,080 --> 00:08:00,160
now we have started our SSH service.

188
00:08:02,639 --> 00:08:05,360
We try again,

189
00:08:05,840 --> 00:08:09,840
now it's working it's asking for

190
00:08:08,160 --> 00:08:10,840
verification,

191
00:08:09,840 --> 00:08:12,479
we

192
00:08:10,840 --> 00:08:15,759
accept

193
00:08:12,479 --> 00:08:20,160
and now we are into our WSL environment.

194
00:08:15,759 --> 00:08:20,160
So, we know the PuTTY profile works now,

195
00:08:20,240 --> 00:08:23,919
great!

196
00:08:22,560 --> 00:08:27,759
We have our

197
00:08:23,919 --> 00:08:29,680
PuTTY profile working. 

198
00:08:26,220 --> 00:08:31,560
And because the env.bat file is only taken

199
00:08:30,000 --> 00:08:35,159
into account during the build phase

200
00:08:31,560 --> 00:08:38,640
using the build.bat script, as you can

201
00:08:35,159 --> 00:08:40,560
see here, we basically need to rerun

202
00:08:38,640 --> 00:08:43,680
build.bat in order to take into

203
00:08:40,560 --> 00:08:46,140
account our modified IP address.

204
00:08:43,680 --> 00:08:48,300
So we're just going to execute build.bat

205
00:08:46,140 --> 00:08:51,180
hello again.

206
00:08:48,300 --> 00:08:53,459
We can see again we are resetting the

207
00:08:51,180 --> 00:08:56,580
environment variable and this time it

208
00:08:53,459 --> 00:09:00,120
successfully pushed the binary onto the

209
00:08:56,580 --> 00:09:02,040
target VM. We can see the address here.

210
00:09:00,120 --> 00:09:04,200
So if in your case you have the wrong

211
00:09:02,040 --> 00:09:07,260
address that doesn't correspond to

212
00:09:04,200 --> 00:09:09,180
your target VM, you need to fix the env.bat

213
00:09:07,260 --> 00:09:12,600
file. Make sure it matches the PuTTY

214
00:09:09,180 --> 00:09:15,800
profile and then restart the build.bat

215
00:09:12,600 --> 00:09:15,800
hello command.

216
00:09:15,839 --> 00:09:21,520
So, now if we go back here,

217
00:09:17,760 --> 00:09:21,520
and build our actual project,

218
00:09:25,599 --> 00:09:31,280
you see that it successfully pushed the

219
00:09:27,760 --> 00:09:31,280
files onto the Desktop.

220
00:09:32,960 --> 00:09:36,960
So, we can see HelloWorld_lab.exe is now here

221
00:09:38,560 --> 00:09:43,359
and we can run it.

222
00:09:41,200 --> 00:09:45,280
So, now let's say we want to modify our

223
00:09:43,359 --> 00:09:48,479
binary

224
00:09:45,280 --> 00:09:51,680
to say "Hello OST2!"

225
00:09:48,479 --> 00:09:51,680
and then we rebuild it,

226
00:09:53,440 --> 00:09:56,960
and then when we go to our target VM and

227
00:09:55,200 --> 00:09:59,760
we rerun it.

228
00:09:56,960 --> 00:10:03,280
We can see we quickly were able to test

229
00:09:59,760 --> 00:10:03,280
it. Okay, thanks for watching.

