1
00:00:00,000 --> 00:00:04,640
The goal here is to

2
00:00:01,839 --> 00:00:07,279
install the ret-sync plugin. The whole

3
00:00:04,640 --> 00:00:09,440
idea of the ret-sync plugin is to be able

4
00:00:07,279 --> 00:00:12,160
to use both a disassembler and

5
00:00:09,440 --> 00:00:14,000
decompiler, as well as a debugger and

6
00:00:12,160 --> 00:00:16,080
synchronize them all together. So, you're

7
00:00:14,000 --> 00:00:18,400
going to be able to use the power of

8
00:00:16,080 --> 00:00:19,920
WinDbg, all the commands and at the

9
00:00:18,400 --> 00:00:22,800
same time, you'll be able to rename

10
00:00:19,920 --> 00:00:25,680
functions into Ghidra or IDA, rename the

11
00:00:22,800 --> 00:00:28,560
variables, make it very nice to read and

12
00:00:25,680 --> 00:00:30,640
still use both the disassembler and WinDbg

13
00:00:28,560 --> 00:00:32,480
at the same time and synchronize

14
00:00:30,640 --> 00:00:34,719
them together. Let's get started. So in

15
00:00:32,480 --> 00:00:37,440
order to use ret-sync, you need to load a

16
00:00:34,719 --> 00:00:39,840
plugin both in the decompiler and in the

17
00:00:37,440 --> 00:00:42,399
the debugger. To do so in the actual

18
00:00:39,840 --> 00:00:45,280
Ghidra or IDA disassembler you're going

19
00:00:42,399 --> 00:00:47,840
to use the Alt+S keys. One thing about

20
00:00:45,280 --> 00:00:50,320
Ghidra is that you need to actually load

21
00:00:47,840 --> 00:00:52,239
all the DLLs, you want to be able to

22
00:00:50,320 --> 00:00:54,239
synchronize with, in the same code

23
00:00:52,239 --> 00:00:56,640
browser window. Another thing about

24
00:00:54,239 --> 00:00:59,680
ret-sync is that the highlighting of the

25
00:00:56,640 --> 00:01:01,920
actual decompiler window line doesn't

26
00:00:59,680 --> 00:01:04,000
work perfectly on Ghidra but it does work

27
00:01:01,920 --> 00:01:05,600
great on IDA Pro, so something to take

28
00:01:04,000 --> 00:01:07,760
into account. In order to install

29
00:01:05,600 --> 00:01:10,720
ret-sync it was actually done already by the

30
00:01:07,760 --> 00:01:13,360
actual script, we downloaded the sources

31
00:01:10,720 --> 00:01:14,799
and we built the actual plugin for Ghidra

32
00:01:13,360 --> 00:01:17,200
automatically. One thing that is

33
00:01:14,799 --> 00:01:19,680
remaining to do is to actually import

34
00:01:17,200 --> 00:01:21,280
the Ghidra extension into Ghidra itself.

35
00:01:19,680 --> 00:01:23,280
We'll do that in a minute. You have to

36
00:01:21,280 --> 00:01:25,600
make sure the plugin version actually

37
00:01:23,280 --> 00:01:27,840
matches the Ghidra version, which is kind

38
00:01:25,600 --> 00:01:29,600
of why we build it automatically during

39
00:01:27,840 --> 00:01:31,360
installation to make sure it's the right

40
00:01:29,600 --> 00:01:33,360
version. This is the kind of thing you

41
00:01:31,360 --> 00:01:36,880
end up having. On the left side you can

42
00:01:33,360 --> 00:01:40,400
see WinDbg, we see that we have loaded

43
00:01:36,880 --> 00:01:42,880
the sync.dll plugin and then we use the

44
00:01:40,400 --> 00:01:45,360
!sync command in order to

45
00:01:42,880 --> 00:01:47,520
synchronize with the Ghidra plugin. We can

46
00:01:45,360 --> 00:01:49,759
see that we are currently just on the

47
00:01:47,520 --> 00:01:51,759
actual call NtWriteFile function. On

48
00:01:49,759 --> 00:01:54,560
the right side we can see Ghidra. We can

49
00:01:51,759 --> 00:01:56,640
see at the bottom that the Ghidra ret-sync

50
00:01:54,560 --> 00:01:58,799
plugin is actually loaded as well. It

51
00:01:56,640 --> 00:02:00,719
found the actual configuration file into

52
00:01:58,799 --> 00:02:03,360
the hello_world.rep

53
00:02:00,719 --> 00:02:05,200
folder and that the kernelbase.dll file

54
00:02:03,360 --> 00:02:08,000
is currently opened. We can also see that

55
00:02:05,200 --> 00:02:09,840
it's been synchronized with the WinDbg

56
00:02:08,000 --> 00:02:12,000
dialog. And because on the left we can

57
00:02:09,840 --> 00:02:13,760
see it's on the call NtWriteFile, we

58
00:02:12,000 --> 00:02:15,840
can also see that on the right into

59
00:02:13,760 --> 00:02:18,319
Ghidra itself on the decompiled code we

60
00:02:15,840 --> 00:02:20,319
are actually on the NtWriteFile call

61
00:02:18,319 --> 00:02:22,480
which is highlighted in yellow. So one

62
00:02:20,319 --> 00:02:24,239
thing that is really annoying at first

63
00:02:22,480 --> 00:02:26,319
with ret-sync is the way the

64
00:02:24,239 --> 00:02:28,080
configuration files work. It can be quite

65
00:02:26,319 --> 00:02:29,760
painful at first, but it's actually worth

66
00:02:28,080 --> 00:02:31,519
doing it once, because then it's very

67
00:02:29,760 --> 00:02:33,440
powerful. The idea is you have to define

68
00:02:31,519 --> 00:02:35,200
the host and the port of the other half.

69
00:02:33,440 --> 00:02:38,000
So for WinDbg, you have to tell it

70
00:02:35,200 --> 00:02:40,480
where Ghidra or IDA can be reached. And

71
00:02:38,000 --> 00:02:42,160
for Ghidra or IDA, you have to tell them where

72
00:02:40,480 --> 00:02:44,640
to reach the debugger, in this case

73
00:02:42,160 --> 00:02:46,800
WinDbg. For the WinDbg extension, it

74
00:02:44,640 --> 00:02:49,200
only looks for the actual configuration

75
00:02:46,800 --> 00:02:51,200
file into the user directory. We can see

76
00:02:49,200 --> 00:02:52,959
it defines the host and port to connect

77
00:02:51,200 --> 00:02:55,200
to. For the actual disassembler and

78
00:02:52,959 --> 00:02:57,280
decompiler, both for Ghidra and IDA,

79
00:02:55,200 --> 00:02:59,599
the ret-sync extension will

80
00:02:57,280 --> 00:03:01,360
actually look first into the project

81
00:02:59,599 --> 00:03:03,280
path and then into the actual user

82
00:03:01,360 --> 00:03:05,120
directory. The extension only takes into

83
00:03:03,280 --> 00:03:07,680
account the first configuration file it

84
00:03:05,120 --> 00:03:09,920
finds. So, for Ghidra, the easiest method is

85
00:03:07,680 --> 00:03:12,239
to actually define a .sync

86
00:03:09,920 --> 00:03:14,800
configuration file into the .rep

87
00:03:12,239 --> 00:03:16,800
directory. We can see one specificity

88
00:03:14,800 --> 00:03:19,280
here is that we define an alias and the

89
00:03:16,800 --> 00:03:21,760
reason is because ntkrnlmp(.exe) is the

90
00:03:19,280 --> 00:03:24,480
actual name defined in WinDbg, but the

91
00:03:21,760 --> 00:03:27,120
actual file itself is named ntoskrnl(.exe)

92
00:03:24,480 --> 00:03:28,720
so it allows ret-sync to know that there

93
00:03:27,120 --> 00:03:31,120
is a match between the debugger and the

94
00:03:28,720 --> 00:03:36,000
disassembler. Okay, so let's install

95
00:03:31,120 --> 00:03:36,000
ret-sync and let's configure it. ret-sync

96
00:03:36,080 --> 00:03:40,319
extension for WinDbg has been

97
00:03:38,319 --> 00:03:43,760
installed already.

98
00:03:40,319 --> 00:03:44,959
The one for Ghidra is in ext_ghidra\

99
00:03:43,760 --> 00:03:47,120
dist\

100
00:03:44,959 --> 00:03:48,879
If you sort them by date,

101
00:03:47,120 --> 00:03:50,799
you'll see that one of them is the

102
00:03:48,879 --> 00:03:52,480
latest version that has been built

103
00:03:50,799 --> 00:03:53,599
automatically. So,

104
00:03:52,480 --> 00:03:55,519
what we're going to do is we're going to

105
00:03:53,599 --> 00:04:00,439
start Ghidra,

106
00:03:55,519 --> 00:04:00,439
by double clicking on ghidraRun.

107
00:04:04,319 --> 00:04:09,280
To install ret-sync, you can follow

108
00:04:06,000 --> 00:04:11,439
instructions on this GitHub page.

109
00:04:09,280 --> 00:04:12,720
Into Ghidra extension, you'll see how

110
00:04:11,439 --> 00:04:16,079
to build the extension we've done

111
00:04:12,720 --> 00:04:17,680
already and how to install it.

112
00:04:16,079 --> 00:04:20,320
But basically, the whole point is to go

113
00:04:17,680 --> 00:04:20,320
into File

114
00:04:21,440 --> 00:04:29,320
Install Extensions...,

115
00:04:24,160 --> 00:04:29,320
and then click on the Add Extension.

116
00:04:31,759 --> 00:04:35,759
So we're going to ret-sync\ext_ghidra\

117
00:04:33,520 --> 00:04:39,479
dist\

118
00:04:35,759 --> 00:04:39,479
and install the...

119
00:04:42,320 --> 00:04:45,199
the highest version.

120
00:04:45,759 --> 00:04:48,800
You see it is ticked here.

121
00:04:50,400 --> 00:04:55,000
It's going to ask us to restart Ghidra.

122
00:04:59,520 --> 00:05:02,560
We restart Ghidra.

123
00:05:06,000 --> 00:05:12,240
Now when the first time we open

124
00:05:08,880 --> 00:05:14,160
an actual CodeBrowser window,

125
00:05:12,240 --> 00:05:16,800
it is going to actually ask us to configure

126
00:05:14,160 --> 00:05:16,800
the extension.

127
00:05:17,280 --> 00:05:20,880
We just say "yes" to that,

128
00:05:21,120 --> 00:05:25,400
then we click the ret-sync plugin,

129
00:05:26,080 --> 00:05:28,800
and click Ok.

130
00:05:33,919 --> 00:05:39,759
So, as you can see there is an additional

131
00:05:36,320 --> 00:05:41,759
window which is related to ret-sync.

132
00:05:39,759 --> 00:05:44,720
The best you can do is to actually drag

133
00:05:41,759 --> 00:05:48,880
and drop the ret-sync plugin tab

134
00:05:44,720 --> 00:05:48,880
into the actual CodeBrowser like this,

135
00:05:50,080 --> 00:05:56,560
and you will see it here from now on.

136
00:05:53,840 --> 00:05:59,360
You shouldn't have to do that again.

137
00:05:56,560 --> 00:06:01,039
So ret-sync is installed into Ghidra

138
00:05:59,360 --> 00:06:03,919
now.

139
00:06:01,039 --> 00:06:06,720
We want to be able to debug and see

140
00:06:03,919 --> 00:06:08,880
ret-sync working in Ghidra for all the DLLs.

141
00:06:06,720 --> 00:06:10,720
So, we drag and drop

142
00:06:08,880 --> 00:06:13,280
the additional DLL into the same

143
00:06:10,720 --> 00:06:13,280
CodeBrowser.

144
00:06:14,240 --> 00:06:19,039
You can see they are listed here.

145
00:06:17,440 --> 00:06:21,680
Now we're going to start

146
00:06:19,039 --> 00:06:23,840
WinDbg and debug the actual

147
00:06:21,680 --> 00:06:26,080
target VM.

148
00:06:23,840 --> 00:06:29,400
So, we're going to start our

149
00:06:26,080 --> 00:06:29,400
batch file,

150
00:06:34,240 --> 00:06:38,400
so we're just going to show you. On the

151
00:06:36,160 --> 00:06:40,960
right, we have Ghidra.

152
00:06:38,400 --> 00:06:42,319
And on the left, we have WinDbg. WinDbg

153
00:06:40,960 --> 00:06:45,039
is connected.

154
00:06:42,319 --> 00:06:45,039
So, we break.

155
00:06:45,680 --> 00:06:51,520
We see we are on

156
00:06:48,800 --> 00:06:53,919
DbgBreakpointWithStatus.

157
00:06:51,520 --> 00:06:55,759
So, in order to load ret-sync into WinDbg

158
00:06:53,919 --> 00:06:59,560
we just do .load

159
00:06:55,759 --> 00:06:59,560
sync

160
00:07:02,319 --> 00:07:07,360
and now we can execute commands like

161
00:07:05,440 --> 00:07:09,360
!sync.

162
00:07:07,360 --> 00:07:12,639
For this to work, it has to be loaded

163
00:07:09,360 --> 00:07:14,720
into Ghidra. If we try right now,

164
00:07:12,639 --> 00:07:16,639
it's not going to work because the Ghidra

165
00:07:14,720 --> 00:07:20,240
plugin is not loaded.

166
00:07:16,639 --> 00:07:20,240
We see the status is idle.

167
00:07:21,520 --> 00:07:26,880
Also, even if I start it here,

168
00:07:24,720 --> 00:07:28,880
and then start again,

169
00:07:26,880 --> 00:07:30,800
we can see it's loaded,

170
00:07:28,880 --> 00:07:32,880
however, we haven't defined any

171
00:07:30,800 --> 00:07:35,280
configuration file at the moment.

172
00:07:32,880 --> 00:07:37,599
So, when it's trying to synchronize the

173
00:07:35,280 --> 00:07:40,680
actual

174
00:07:37,599 --> 00:07:42,800
ntkrnlmp.exe, it's not synchronized with

175
00:07:40,680 --> 00:07:44,479
ntoskrnl.exe, because there isn't a match

176
00:07:42,800 --> 00:07:46,479
between the names.

177
00:07:44,479 --> 00:07:47,599
So,

178
00:07:46,479 --> 00:07:49,520
we

179
00:07:47,599 --> 00:07:52,400
disable synchronization

180
00:07:49,520 --> 00:07:53,840
and we have to restart Ghidra

181
00:07:52,400 --> 00:07:56,080
after adding the

182
00:07:53,840 --> 00:07:57,120
configuration files. So, the actual

183
00:07:56,080 --> 00:08:00,240
project

184
00:07:57,120 --> 00:08:02,319
is hello_world.rep.

185
00:08:00,240 --> 00:08:04,639
We provide two

186
00:08:02,319 --> 00:08:06,160
config files, one is to save into the

187
00:08:04,639 --> 00:08:08,560
user folder and the other one to the

188
00:08:06,160 --> 00:08:13,479
Ghidra project. So, let's start with the

189
00:08:08,560 --> 00:08:13,479
IEuser one. So, we go into

190
00:08:13,840 --> 00:08:18,080
C:\users\

191
00:08:15,759 --> 00:08:19,599
IEuser\ and paste

192
00:08:18,080 --> 00:08:23,039
that .sync

193
00:08:19,599 --> 00:08:23,039
config file, the one from

194
00:08:23,280 --> 00:08:27,520
tools\IEuser.

195
00:08:25,280 --> 00:08:31,199
There is one other

196
00:08:27,520 --> 00:08:33,200
configuration file into the folder.rep

197
00:08:31,199 --> 00:08:35,919
folder which we can see define the

198
00:08:33,200 --> 00:08:35,919
actual alias.

199
00:08:36,159 --> 00:08:41,760
So, this one needs to be copied into

200
00:08:39,200 --> 00:08:43,760
the .rep folder corresponding to the

201
00:08:41,760 --> 00:08:47,800
project you are debugging. Okay, so now

202
00:08:43,760 --> 00:08:47,800
we're ready to restart Ghidra.

203
00:09:13,440 --> 00:09:19,760
So, we have loaded our three binaries

204
00:09:16,320 --> 00:09:22,560
into the same CodeBrowser.

205
00:09:19,760 --> 00:09:25,040
Now, we can re-execute the !sync command

206
00:09:22,560 --> 00:09:29,600
but we need first to enable the ret-sync

207
00:09:25,040 --> 00:09:31,600
plugin into Ghidra, so we do Alt+S.

208
00:09:29,600 --> 00:09:34,240
This change to listening,

209
00:09:31,600 --> 00:09:37,120
here we can see that it actually loaded

210
00:09:34,240 --> 00:09:39,120
the configuration file from our project

211
00:09:37,120 --> 00:09:42,560
file. So, there is an alias between ntoskrnl and

212
00:09:39,120 --> 00:09:45,200
ntkrnlmp.

213
00:09:42,560 --> 00:09:48,160
So, now if we synchronize,

214
00:09:45,200 --> 00:09:50,480
we see that it actually switched to the

215
00:09:48,160 --> 00:09:52,720
actual code, so

216
00:09:50,480 --> 00:09:56,000
here we are

217
00:09:52,720 --> 00:09:58,080
on to DbgBreakpointWithStatus

218
00:09:56,000 --> 00:10:00,640
and we can see it automatically changed

219
00:09:58,080 --> 00:10:01,920
on the Ghidra window to this actual

220
00:10:00,640 --> 00:10:04,560
function.

221
00:10:01,920 --> 00:10:05,839
And if we set a breakpoint onto

222
00:10:04,560 --> 00:10:08,720


223
00:10:05,839 --> 00:10:08,720
NtReadFile

224
00:10:09,200 --> 00:10:15,480
and then continue

225
00:10:11,519 --> 00:10:15,480
and wait until it hits.

226
00:10:28,320 --> 00:10:33,920
Okay, so we hit the NtReadFile function

227
00:10:31,440 --> 00:10:35,920
and we can see that automatically into

228
00:10:33,920 --> 00:10:37,279
Ghidra, it's actually switching to this

229
00:10:35,920 --> 00:10:40,079
function

230
00:10:37,279 --> 00:10:41,760
and we can even step into

231
00:10:40,079 --> 00:10:43,440
WinDbg

232
00:10:41,760 --> 00:10:44,880
and it will step automatically into

233
00:10:43,440 --> 00:10:47,839
Ghidra,

234
00:10:44,880 --> 00:10:47,839
so let's do that.

235
00:10:48,480 --> 00:10:52,880
As you can see on the right

236
00:10:50,640 --> 00:10:54,000
it's stepping into the function. So, here

237
00:10:52,880 --> 00:10:55,279
for instance I'm going to set a

238
00:10:54,000 --> 00:10:57,040
breakpoint

239
00:10:55,279 --> 00:10:58,800
on this 

240
00:10:57,040 --> 00:11:03,360
ObReferenceByHandle call

241
00:10:58,800 --> 00:11:04,480
I'm going to go on Ghidra and use Alt+F3

242
00:11:03,360 --> 00:11:05,839
you can see on the left that

243
00:11:04,480 --> 00:11:07,920
automatically it actually sets a

244
00:11:05,839 --> 00:11:11,120
breakpoint on the right address.

245
00:11:07,920 --> 00:11:11,120
If we continue execution,

246
00:11:12,640 --> 00:11:17,600
we can see that we actually reached

247
00:11:15,600 --> 00:11:20,640
this call ObReferenceByHandle.

248
00:11:17,600 --> 00:11:23,200
So, we provide two scripts the

249
00:11:20,640 --> 00:11:25,920
first one is a batch script which allows

250
00:11:23,200 --> 00:11:27,360
us to start WinDbg and this batch

251
00:11:25,920 --> 00:11:29,920
script will actually

252
00:11:27,360 --> 00:11:32,000
load a bunch of commands at startup. The

253
00:11:29,920 --> 00:11:34,000
thing we can automate are things like

254
00:11:32,000 --> 00:11:36,160
starting ret-sync, synchronizing with

255
00:11:34,000 --> 00:11:38,640
the actual disassembler, in our case

256
00:11:36,160 --> 00:11:40,480
Ghidra, reload symbols and set

257
00:11:38,640 --> 00:11:42,480
breakpoints, so we don't have to set them

258
00:11:40,480 --> 00:11:44,640
manually each time we restart our

259
00:11:42,480 --> 00:11:48,320
debugging session. Now that we have seen

260
00:11:44,640 --> 00:11:50,240
how to configure ret-sync for WinDbg and

261
00:11:48,320 --> 00:11:54,079
Ghidra we're going to see how to automate

262
00:11:50,240 --> 00:11:54,079
doing it on the WinDbg side.

263
00:11:55,120 --> 00:11:59,839
If we open these two files,

264
00:11:58,160 --> 00:12:01,920
we see that the bat is the one we

265
00:11:59,839 --> 00:12:04,079
execute to start WinDbg, and actually

266
00:12:01,920 --> 00:12:06,880
there is a way to execute an external

267
00:12:04,079 --> 00:12:08,880
cmd file that contains WinDbg commands.

268
00:12:06,880 --> 00:12:11,440
So, we uncomment that one and we can

269
00:12:08,880 --> 00:12:13,519
comment the other one to execute

270
00:12:11,440 --> 00:12:15,760
commands when we start WinDbg with that

271
00:12:13,519 --> 00:12:17,920
script.

272
00:12:15,760 --> 00:12:20,399
This lists the command that we want to

273
00:12:17,920 --> 00:12:22,560
execute, we can see we're going to load ret-sync

274
00:12:20,399 --> 00:12:24,800
in WinDbg

275
00:12:22,560 --> 00:12:26,880
then we're going to synchronize with Ghidra

276
00:12:24,800 --> 00:12:29,120
then we're going to reload symbols for

277
00:12:26,880 --> 00:12:31,519
specific modules. Here I'm going to

278
00:12:29,120 --> 00:12:34,079
uncomment setting a breakpoint

279
00:12:31,519 --> 00:12:36,639
on NtReadFile as an example.

280
00:12:34,079 --> 00:12:38,399
And then I'm going to list the breakpoints

281
00:12:36,639 --> 00:12:41,760
to make sure everything is sane and then

282
00:12:38,399 --> 00:12:41,760
continue execution with go.

283
00:12:43,120 --> 00:12:46,880
On Ghidra,

284
00:12:44,320 --> 00:12:49,279
we can see that we have loaded

285
00:12:46,880 --> 00:12:50,800
all the modules into the same code

286
00:12:49,279 --> 00:12:54,079
browser, on top of that we're going to

287
00:12:50,800 --> 00:12:56,720
start the ret-sync plugin using Alt+S, it's

288
00:12:54,079 --> 00:12:56,720
now listening

289
00:12:57,360 --> 00:13:02,079
the configuration file has been loaded.

290
00:12:59,680 --> 00:13:04,240
So, we're ready. So, now we start the bat

291
00:13:02,079 --> 00:13:07,720
script that's going to execute our

292
00:13:04,240 --> 00:13:07,720
dbg-prep.cmd,

293
00:13:13,600 --> 00:13:16,959
it's connecting to the target.

294
00:13:17,519 --> 00:13:20,639
Now, we can break

295
00:13:19,519 --> 00:13:22,800
and when we break it's going to

296
00:13:20,639 --> 00:13:24,800
automatically execute the commands.

297
00:13:22,800 --> 00:13:27,839
So, we can see

298
00:13:24,800 --> 00:13:30,320
it loaded ret-sync, then it synchronized

299
00:13:27,839 --> 00:13:31,680
with Ghidra,

300
00:13:30,320 --> 00:13:33,760
and now we see

301
00:13:31,680 --> 00:13:36,720
automatically broke on the actual

302
00:13:33,760 --> 00:13:36,720
NtReadFile,

303
00:13:37,920 --> 00:13:42,320
and we can see in Ghidra we're already

304
00:13:39,920 --> 00:13:44,399
synchronized. Okay, we have a good

305
00:13:42,320 --> 00:13:46,800
synchronization between our debugger and

306
00:13:44,399 --> 00:13:49,199
our decompiler disassembler that's good

307
00:13:46,800 --> 00:13:49,199
for now.

