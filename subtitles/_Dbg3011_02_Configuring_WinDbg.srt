1
00:00:00,320 --> 00:00:04,799
Hi everyone, in this part we're going to

2
00:00:01,920 --> 00:00:06,399
show you how to debug a Windows

3
00:00:04,799 --> 00:00:07,919
environment using 3 different

4
00:00:06,399 --> 00:00:10,639
techniques. We're going to show you how

5
00:00:07,919 --> 00:00:13,759
to Debug it using VirtualKD, using

6
00:00:10,639 --> 00:00:15,920
Network Debugging and using Serial Ports.

7
00:00:13,759 --> 00:00:18,800
We're also going to show you how to

8
00:00:15,920 --> 00:00:20,960
configure symbols. Let's get started. The

9
00:00:18,800 --> 00:00:22,960
first thing I want to say on this

10
00:00:20,960 --> 00:00:25,359
Windows kernel debugger is that some

11
00:00:22,960 --> 00:00:28,240
people name it "WinDbg" some other people

12
00:00:25,359 --> 00:00:31,279
name it "WinDebug" and some other people

13
00:00:28,240 --> 00:00:33,680
name it "WindBag" like a Wind-Bag. So it's

14
00:00:31,279 --> 00:00:35,920
basically the only real debugger to

15
00:00:33,680 --> 00:00:37,920
debug the Windows kernel there are two

16
00:00:35,920 --> 00:00:40,079
alternatives, the first one is IDA Pro

17
00:00:37,920 --> 00:00:41,760
which has a good interface to debug the

18
00:00:40,079 --> 00:00:42,960
Windows kernel apparently, but we're not

19
00:00:41,760 --> 00:00:45,520
going to use that technique because

20
00:00:42,960 --> 00:00:47,520
we're going to basically use WinDbg

21
00:00:45,520 --> 00:00:50,000
synchronized with a disassembler, with a

22
00:00:47,520 --> 00:00:52,160
plug-in, as we'll see later. And the

23
00:00:50,000 --> 00:00:54,480
other technique is to

24
00:00:52,160 --> 00:00:56,399
use a HyperDbg which is like the new

25
00:00:54,480 --> 00:00:58,079
promising replacement. But, for this

26
00:00:56,399 --> 00:00:59,440
presentation we're going to focus on WinDbg.

27
00:00:58,079 --> 00:01:00,960
So

28
00:00:59,440 --> 00:01:03,680
there has been a few versions of WinDbg.

29
00:01:00,960 --> 00:01:05,840
The first one was WinDbg,

30
00:01:03,680 --> 00:01:08,080
the classical WinDbg, and now you have

31
00:01:05,840 --> 00:01:10,400
WinDbg Preview. So, really, what you want

32
00:01:08,080 --> 00:01:12,320
is mostly to use WinDbg Preview in

33
00:01:10,400 --> 00:01:15,040
general, because it supports JavaScript

34
00:01:12,320 --> 00:01:16,320
scripting which allows you to

35
00:01:15,040 --> 00:01:19,200
write scripts,

36
00:01:16,320 --> 00:01:21,439
a lot easier than using the old WinDbg

37
00:01:19,200 --> 00:01:23,920
commands. It also supports a Windows

38
00:01:21,439 --> 00:01:27,360
layout which is out of the box which is

39
00:01:23,920 --> 00:01:29,040
way better than the actual single window

40
00:01:27,360 --> 00:01:31,439
in the original WinDbg, it's also

41
00:01:29,040 --> 00:01:33,200
generally faster than the old WinDbg

42
00:01:31,439 --> 00:01:34,960
and so generally you want to use it.

43
00:01:33,200 --> 00:01:36,960
There are some cases where you can't use

44
00:01:34,960 --> 00:01:39,600
it, for instance, if you try to

45
00:01:36,960 --> 00:01:41,680
debug really old Windows versions, things

46
00:01:39,600 --> 00:01:43,840
may break structures may fail to be

47
00:01:41,680 --> 00:01:45,840
printed or the debugger may just fail to

48
00:01:43,840 --> 00:01:47,280
work properly. So, then, in this case

49
00:01:45,840 --> 00:01:49,119
you would just want to roll back to

50
00:01:47,280 --> 00:01:51,600
using the old WinDbg. There are 3

51
00:01:49,119 --> 00:01:54,399
kernel debugging approaches: VirtualKD,

52
00:01:51,600 --> 00:01:56,560
Network and Serial Debugging. VirtualKD

53
00:01:54,399 --> 00:01:58,719
is the recommended one, it's the

54
00:01:56,560 --> 00:02:00,880
fastest method if you have a Windows

55
00:01:58,719 --> 00:02:03,680
host. It won't work though if you have OSX

56
00:02:00,880 --> 00:02:05,759
or Linux as a host. The second method

57
00:02:03,680 --> 00:02:08,080
is using network, so you're going to have

58
00:02:05,759 --> 00:02:09,599
two Windows VMs one is the debugger VM

59
00:02:08,080 --> 00:02:11,840
and the other one is the target VM, but

60
00:02:09,599 --> 00:02:14,720
this method will only work if you're

61
00:02:11,840 --> 00:02:16,400
debugging a target VM with Windows 8 or

62
00:02:14,720 --> 00:02:19,120
above, so for instance it won't work if

63
00:02:16,400 --> 00:02:21,520
you're debugging Windows 7 or Windows XP.

64
00:02:19,120 --> 00:02:23,280
The third method is using serial ports,

65
00:02:21,520 --> 00:02:25,680
again you're going to use two virtual

66
00:02:23,280 --> 00:02:28,080
machines one debugger VM and the other

67
00:02:25,680 --> 00:02:30,400
one target VM. It's basically the slower

68
00:02:28,080 --> 00:02:32,640
method, so you would only want to use

69
00:02:30,400 --> 00:02:34,720
that method if you're actually debugging

70
00:02:32,640 --> 00:02:36,879
an old Windows. So, in our case we're

71
00:02:34,720 --> 00:02:40,560
going to use two virtual machines and we're

72
00:02:36,879 --> 00:02:40,560
going to use network kernel debugging.

73
00:02:40,879 --> 00:02:45,040
So, the first method is VirtualKD.

74
00:02:42,959 --> 00:02:47,120
VirtualKD is actually a very nice

75
00:02:45,040 --> 00:02:49,040
method there has been a fork of VirtualKD

76
00:02:47,120 --> 00:02:50,560
which is called VirtualKD-Redux,

77
00:02:49,040 --> 00:02:52,239
which is maintained, so I would recommend

78
00:02:50,560 --> 00:02:55,519
using that. And you just follow the

79
00:02:52,239 --> 00:02:58,800
tutorial which is basically to copy some

80
00:02:55,519 --> 00:03:00,239
tools on the target VM, run the install

81
00:02:58,800 --> 00:03:02,080
executable and then it's going to

82
00:03:00,239 --> 00:03:03,519
basically install the driver and then you

83
00:03:02,080 --> 00:03:06,959
reboot that virtual machine you're

84
00:03:03,519 --> 00:03:08,959
trying to debug and at boot you hit F8

85
00:03:06,959 --> 00:03:11,200
in order to disable the driver

86
00:03:08,959 --> 00:03:14,000
signatures at boot. Then, on the host

87
00:03:11,200 --> 00:03:15,760
you're going to run a vmmon executable,

88
00:03:14,000 --> 00:03:18,720
and you're going to have to make sure it

89
00:03:15,760 --> 00:03:20,879
actually shows that the VM is detected.

90
00:03:18,720 --> 00:03:21,840
So, the way you're going to see that is that

91
00:03:20,879 --> 00:03:23,360
you have

92
00:03:21,840 --> 00:03:25,280
"Yes" in the

93
00:03:23,360 --> 00:03:26,560
operating system column, so in this case

94
00:03:25,280 --> 00:03:28,720
we started

95
00:03:26,560 --> 00:03:30,319
VMware,

96
00:03:28,720 --> 00:03:33,040
like, the name of the machine is

97
00:03:30,319 --> 00:03:34,799
Vulnerable_VM and it created a pipe

98
00:03:33,040 --> 00:03:37,120
kd_Vulnerable_VM

99
00:03:34,799 --> 00:03:39,280
and in front of that, we see a "Yes", so

100
00:03:37,120 --> 00:03:42,159
that means VirtualKD was successfully

101
00:03:39,280 --> 00:03:44,959
installed in the VM and we're ready to do

102
00:03:42,159 --> 00:03:46,640
the next step. If you don't see a "Yes", it

103
00:03:44,959 --> 00:03:48,640
means something went wrong during

104
00:03:46,640 --> 00:03:50,799
installation, so in this case you're

105
00:03:48,640 --> 00:03:53,599
going to have to follow the manual

106
00:03:50,799 --> 00:03:56,720
instructions to copy the DLLs and

107
00:03:53,599 --> 00:03:58,560
configure the debug settings manually. So,

108
00:03:56,720 --> 00:04:01,120
once you reboot the VM, as I said, you're

109
00:03:58,560 --> 00:04:03,599
going to have to disable the signature so

110
00:04:01,120 --> 00:04:05,599
using F8 and then selecting this

111
00:04:03,599 --> 00:04:07,120
Disable Signature Enforcement Manually!!!

112
00:04:05,599 --> 00:04:09,280
and then after that, you're going to have

113
00:04:07,120 --> 00:04:11,599
to select Disable Driver Signature

114
00:04:09,280 --> 00:04:14,000
Enforcement. So, after doing that you're

115
00:04:11,599 --> 00:04:17,280
going to see that not only the VirtualKD

116
00:04:14,000 --> 00:04:19,359
shows "Yes" for the OS, but once you attach

117
00:04:17,280 --> 00:04:21,280
the debugger, it's going to actually show

118
00:04:19,359 --> 00:04:23,120
"Yes" in front of the Debugger column. So,

119
00:04:21,280 --> 00:04:24,639
how do we attach the debugger? So, on the

120
00:04:23,120 --> 00:04:26,639
host you're going to run

121
00:04:24,639 --> 00:04:29,360
WinDbg Preview

122
00:04:26,639 --> 00:04:30,800
and then select File > Attach to kernel

123
00:04:29,360 --> 00:04:33,199
and then you're going to have to fill in

124
00:04:30,800 --> 00:04:35,040
the pipe information kd_ the

125
00:04:33,199 --> 00:04:36,639
name of the virtual machine as well at

126
00:04:35,040 --> 00:04:38,720
the baud rate.

127
00:04:36,639 --> 00:04:42,479
Once you do that, it's going to actually

128
00:04:38,720 --> 00:04:44,800
show you the debugger "Yes". But, sure, it's

129
00:04:42,479 --> 00:04:45,840
annoying to do that manually so the

130
00:04:44,800 --> 00:04:48,080
fastest

131
00:04:45,840 --> 00:04:50,400
way is to use a batch script, especially

132
00:04:48,080 --> 00:04:52,560
if you're debugging more than one target

133
00:04:50,400 --> 00:04:55,199
like if you're trying to debug something

134
00:04:52,560 --> 00:04:57,840
on different VMs, you can create one

135
00:04:55,199 --> 00:05:00,479
batch script for each of your virtual

136
00:04:57,840 --> 00:05:03,840
machine and use the path to WinDbg and

137
00:05:00,479 --> 00:05:06,080
specifying the actual pipe for that VM

138
00:05:03,840 --> 00:05:07,840
and then you save that into a debug_ the

139
00:05:06,080 --> 00:05:10,479
Windows machine you're targeting. So we

140
00:05:07,840 --> 00:05:12,240
do provide a batch script

141
00:05:10,479 --> 00:05:15,199
that you can use if you're using VirtualKD.

142
00:05:12,240 --> 00:05:17,039
The next method we are going to talk about

143
00:05:15,199 --> 00:05:18,960
is network debugging. This is the one

144
00:05:17,039 --> 00:05:22,560
we're going to use in this training. So, the

145
00:05:18,960 --> 00:05:24,400
method relies on a shared key, like a

146
00:05:22,560 --> 00:05:27,120
secret between the debugger VM and the

147
00:05:24,400 --> 00:05:29,120
target VM, and also on a port number

148
00:05:27,120 --> 00:05:31,199
defined to reach the target VM. In this

149
00:05:29,120 --> 00:05:34,000
scenario, one is the client and the other

150
00:05:31,199 --> 00:05:36,320
one is the server, so on your debugger VM

151
00:05:34,000 --> 00:05:38,000
you're going to have to find your Host-Only

152
00:05:36,320 --> 00:05:40,800
IP address, it should be the second

153
00:05:38,000 --> 00:05:42,960
interface listed and you can check

154
00:05:40,800 --> 00:05:44,960
against the VMware Virtual Network

155
00:05:42,960 --> 00:05:47,440
Editor that it actually corresponds to

156
00:05:44,960 --> 00:05:49,680
the Host-Only interface. Once you have

157
00:05:47,440 --> 00:05:52,080
that IP, basically what you do is you go

158
00:05:49,680 --> 00:05:54,880
on the target VM and you enable kernel

159
00:05:52,080 --> 00:05:57,360
debugging. So, you can execute these two

160
00:05:54,880 --> 00:05:59,919
bcdedit commands and we see in the

161
00:05:57,360 --> 00:06:01,440
second one, we specify the key which is

162
00:05:59,919 --> 00:06:03,919
kind of arbitrary. We just need to

163
00:06:01,440 --> 00:06:06,560
remember it. It can be a.b.c.d

164
00:06:03,919 --> 00:06:09,120
and then you specify the host IP which

165
00:06:06,560 --> 00:06:10,880
is the IP you got from the first step on

166
00:06:09,120 --> 00:06:12,479
the debugger VM. Note that, it doesn't

167
00:06:10,880 --> 00:06:14,319
create any additional line when Windows

168
00:06:12,479 --> 00:06:16,160
boot. So, once you've done that and you

169
00:06:14,319 --> 00:06:19,120
actually reboot the target VM you should

170
00:06:16,160 --> 00:06:21,280
actually expect to see Windows hanging

171
00:06:19,120 --> 00:06:23,199
and not continuing booting and it's

172
00:06:21,280 --> 00:06:24,720
going to do that for a couple of seconds

173
00:06:23,199 --> 00:06:27,280
or maybe a minute

174
00:06:24,720 --> 00:06:29,199
until you actually connect with WinDbg.

175
00:06:27,280 --> 00:06:30,960
However, if you don't connect right away

176
00:06:29,199 --> 00:06:32,560
it will actually continue booting but

177
00:06:30,960 --> 00:06:35,120
with the kernel debugger attached so you

178
00:06:32,560 --> 00:06:37,360
can still attach later after the target

179
00:06:35,120 --> 00:06:39,680
VM has booted. But the fact that it hangs

180
00:06:37,360 --> 00:06:41,360
is a good method to make sure kernel

181
00:06:39,680 --> 00:06:43,360
debugging was configured correctly on

182
00:06:41,360 --> 00:06:45,280
the target VM. Once the target VM is

183
00:06:43,360 --> 00:06:47,680
waiting for you, to attach you can again

184
00:06:45,280 --> 00:06:49,840
use WinDbg > Attach to kernel and then

185
00:06:47,680 --> 00:06:52,080
you specify the port number as well as

186
00:06:49,840 --> 00:06:54,639
the key and you don't need to specify a

187
00:06:52,080 --> 00:06:56,639
target VM IP address, it could be that

188
00:06:54,639 --> 00:06:58,400
when you attach with WinDbg, it's going

189
00:06:56,639 --> 00:07:01,599
to show you something like that. So, it

190
00:06:58,400 --> 00:07:04,240
doesn't give you any way to input

191
00:07:01,599 --> 00:07:05,440
commands, but it's still working and if

192
00:07:04,240 --> 00:07:07,199
you hit "break"

193
00:07:05,440 --> 00:07:09,280
it will actually show you this kind of

194
00:07:07,199 --> 00:07:11,599
thing where you see that it actually

195
00:07:09,280 --> 00:07:13,599
works and it was connected successfully.

196
00:07:11,599 --> 00:07:16,319
So, we can automate that with a batch

197
00:07:13,599 --> 00:07:19,120
script using this command. And again we

198
00:07:16,319 --> 00:07:21,199
provide a batch file that you can use

199
00:07:19,120 --> 00:07:23,680
from the debugger VM. It could be that

200
00:07:21,199 --> 00:07:26,080
when you actually attach to

201
00:07:23,680 --> 00:07:28,240
the target VM

202
00:07:26,080 --> 00:07:30,880
with the actual debugger VM, it could be

203
00:07:28,240 --> 00:07:33,199
that the target VM hangs, if that happens,

204
00:07:30,880 --> 00:07:36,080
to work around it, you can basically

205
00:07:33,199 --> 00:07:38,160
disconnect the kernel debugger and

206
00:07:36,080 --> 00:07:40,080
then reboot the target VM and then try

207
00:07:38,160 --> 00:07:41,840
again reconnect the kernel debugger. And

208
00:07:40,080 --> 00:07:43,599
at some point it should work. So, the

209
00:07:41,840 --> 00:07:45,199
third method is to use Serial

210
00:07:43,599 --> 00:07:48,479
Debugging. In this case, we're going to

211
00:07:45,199 --> 00:07:50,639
use a shared named pipe between both the

212
00:07:48,479 --> 00:07:52,080
debugger VM and the target VM. So, again

213
00:07:50,639 --> 00:07:54,319
one is the client and the other one in

214
00:07:52,080 --> 00:07:56,240
the server. So in order to do so, we have

215
00:07:54,319 --> 00:07:59,280
to shut down both virtual machines and

216
00:07:56,240 --> 00:08:01,759
configure a serial port to both settings.

217
00:07:59,280 --> 00:08:04,160
We use a similar approach for Linux and

218
00:08:01,759 --> 00:08:06,639
Windows, the difference is just the name

219
00:08:04,160 --> 00:08:09,440
of the pipe that is OS specific. So, for

220
00:08:06,639 --> 00:08:12,080
Windows, we can see we use

221
00:08:09,440 --> 00:08:14,240
\\. syntax and then the name

222
00:08:12,080 --> 00:08:16,160
of the pipe. Again, we use the same

223
00:08:14,240 --> 00:08:18,160
pipe for both of them and one of them

224
00:08:16,160 --> 00:08:21,680
the client and the other one the server.

225
00:08:18,160 --> 00:08:23,919
And for Linux, we can use a named socket

226
00:08:21,680 --> 00:08:25,680
on the file system. Again, one is the

227
00:08:23,919 --> 00:08:28,160
server and the other one is a client. So

228
00:08:25,680 --> 00:08:30,879
then, we boot the target VM and we

229
00:08:28,160 --> 00:08:33,440
configure kernel debugging using the serial

230
00:08:30,879 --> 00:08:36,080
method and the baud rate. Here, the debug

231
00:08:33,440 --> 00:08:37,680
port is 1 if you only have one serial

232
00:08:36,080 --> 00:08:39,360
port, but it could be that you have

233
00:08:37,680 --> 00:08:41,360
several serial ports and then you have to

234
00:08:39,360 --> 00:08:43,440
adapt the debug port number. Again it's

235
00:08:41,360 --> 00:08:45,360
not going to show any additional line

236
00:08:43,440 --> 00:08:47,279
when Windows boots. So, when it boots it

237
00:08:45,360 --> 00:08:49,040
should hang again on the Windows boot

238
00:08:47,279 --> 00:08:52,000
screen until you actually connect with

239
00:08:49,040 --> 00:08:54,320
the debugger VM. So, from the debugger VM,

240
00:08:52,000 --> 00:08:56,640
we again use the WinDbg Preview

241
00:08:54,320 --> 00:08:58,800
"attach to kernel" method and fill the

242
00:08:56,640 --> 00:09:01,200
port information like the port. This port

243
00:08:58,800 --> 00:09:02,720
is the one for the debugger VM. And again

244
00:09:01,200 --> 00:09:04,959
we can automate that using a batch

245
00:09:02,720 --> 00:09:06,880
script that we provide in the tools. So,

246
00:09:04,959 --> 00:09:08,720
if you get this kind of window when

247
00:09:06,880 --> 00:09:10,640
trying to attach, you can hit break and

248
00:09:08,720 --> 00:09:12,480
it will force it to connect. You will end

249
00:09:10,640 --> 00:09:14,080
up having something like that. So, the

250
00:09:12,480 --> 00:09:16,399
last thing we want to do is we want to

251
00:09:14,080 --> 00:09:19,279
enable symbols. Indeed, if you are

252
00:09:16,399 --> 00:09:21,279
debugging Windows binaries, by default

253
00:09:19,279 --> 00:09:23,760
you won't have any idea of the

254
00:09:21,279 --> 00:09:25,519
structures, of the actual functions being

255
00:09:23,760 --> 00:09:27,600
called. Enabling symbols is very

256
00:09:25,519 --> 00:09:30,080
important. You can do so manually in WinDbg

257
00:09:27,600 --> 00:09:31,600
using the .sympath command it's not

258
00:09:30,080 --> 00:09:33,120
going to be persistent, but it's going to

259
00:09:31,600 --> 00:09:35,360
work right away and you're going to be

260
00:09:33,120 --> 00:09:37,440
able to see the changes and the symbols

261
00:09:35,360 --> 00:09:39,360
after using the .reload command. But

262
00:09:37,440 --> 00:09:41,600
really the best method is to actually

263
00:09:39,360 --> 00:09:43,440
define an environment variable, because

264
00:09:41,600 --> 00:09:45,040
then you don't need to do it every time

265
00:09:43,440 --> 00:09:47,680
you start WinDbg and it will be

266
00:09:45,040 --> 00:09:49,760
persistent among reboots. It has

267
00:09:47,680 --> 00:09:51,279
already been done by the

268
00:09:49,760 --> 00:09:53,760
installer scripts, so you don't have to

269
00:09:51,279 --> 00:09:56,880
do it yourself. We can see that we booted

270
00:09:53,760 --> 00:09:58,959
our target VM as well as our debugger VM

271
00:09:56,880 --> 00:10:01,120
already. So, here we're going to want to

272
00:09:58,959 --> 00:10:03,680
debug the target VM

273
00:10:01,120 --> 00:10:05,519
from the actual debugger VM where we run

274
00:10:03,680 --> 00:10:07,920
WinDbg.

275
00:10:05,519 --> 00:10:10,480
So to do so we're going to have to

276
00:10:07,920 --> 00:10:13,680
configure kernel debugging on the target

277
00:10:10,480 --> 00:10:16,079
VM and then attach from the debugger VM

278
00:10:13,680 --> 00:10:19,040
with WinDbg so in order to do so we

279
00:10:16,079 --> 00:10:20,800
need to first get the actual IP address

280
00:10:19,040 --> 00:10:23,120
of the debugger VM.

281
00:10:20,800 --> 00:10:25,440
So, if we do ipconfig on the debugger VM

282
00:10:23,120 --> 00:10:27,760
we're going to see two interfaces. These

283
00:10:25,440 --> 00:10:30,320
interfaces correspond to the two

284
00:10:27,760 --> 00:10:33,040
interfaces of the VM we can find with

285
00:10:30,320 --> 00:10:34,880
Ctrl+D.

286
00:10:33,040 --> 00:10:36,399
So, with Ctrl+D, we see the first one

287
00:10:34,880 --> 00:10:38,000
is the NAT interface, the second one is

288
00:10:36,399 --> 00:10:39,120
the Host-Only interface,

289
00:10:38,000 --> 00:10:41,040
and so

290
00:10:39,120 --> 00:10:42,480
we are interested in the second one

291
00:10:41,040 --> 00:10:44,240
because that's the network shared with

292
00:10:42,480 --> 00:10:48,000
the target VM.

293
00:10:44,240 --> 00:10:50,959
So, the second one is 92.132.

294
00:10:48,000 --> 00:10:54,480
We can confirm that it corresponds to

295
00:10:50,959 --> 00:10:57,120
the host interface by going into Edit >

296
00:10:54,480 --> 00:11:01,360
Virtual Network Editor, and it will show you

297
00:10:57,120 --> 00:11:03,839
that the Host-Only is in the range 92.0

298
00:11:01,360 --> 00:11:05,839
which corresponds to the second one. Okay,

299
00:11:03,839 --> 00:11:07,120
so now that we have the IP address, we're

300
00:11:05,839 --> 00:11:09,680
going to go into

301
00:11:07,120 --> 00:11:11,600
the actual target VM and we're going to start

302
00:11:09,680 --> 00:11:14,600
an Administrator

303
00:11:11,600 --> 00:11:14,600
cmd,

304
00:11:16,959 --> 00:11:22,079
and from that, we're going to use the bcdedit

305
00:11:19,920 --> 00:11:24,959
/debug on command and then we're

306
00:11:22,079 --> 00:11:26,880
going to use the bcdedit /dbgsettings. So,

307
00:11:24,959 --> 00:11:29,360
here we're going to use the

308
00:11:26,880 --> 00:11:32,720
port and key provided.

309
00:11:29,360 --> 00:11:34,320
However, we need to change the IP address

310
00:11:32,720 --> 00:11:37,279
to be the one for

311
00:11:34,320 --> 00:11:37,279
the debugger VM.

312
00:11:37,680 --> 00:11:42,000
Now, it should be done. We can reboot the

313
00:11:39,920 --> 00:11:44,160
target VM. So, here as you can see it's

314
00:11:42,000 --> 00:11:45,760
actually hanging on the blue window.

315
00:11:44,160 --> 00:11:48,240
There is nothing happening. You can see

316
00:11:45,760 --> 00:11:50,079
here it's actually hanging. So, if we are

317
00:11:48,240 --> 00:11:53,600
really fast we can attach with the

318
00:11:50,079 --> 00:11:56,079
actual debugger VM,

319
00:11:53,600 --> 00:11:59,600
going into tools and here you'll find

320
00:11:56,079 --> 00:11:59,600
the debugger VM network.

321
00:11:59,760 --> 00:12:06,240
If you look at that,

322
00:12:02,160 --> 00:12:10,519
you'll see that it actually uses WinDbg

323
00:12:06,240 --> 00:12:10,519
and the network method.

324
00:12:11,120 --> 00:12:14,800
So, the target VM is hanging.

325
00:12:15,360 --> 00:12:20,440
From the debugger VM, we're going to

326
00:12:16,800 --> 00:12:20,440
connect over the network.

327
00:12:26,079 --> 00:12:33,360
It's saying Waiting to reconnect,

328
00:12:29,360 --> 00:12:33,360
the target VM is still hanging.

329
00:12:36,399 --> 00:12:40,079
So, if we hit Break

330
00:12:38,959 --> 00:12:43,600
it may

331
00:12:40,079 --> 00:12:43,600
wake up the target VM.

332
00:12:44,000 --> 00:12:47,760
So, we see it's not working

333
00:12:46,079 --> 00:12:51,079
actually, now it's working it's just a

334
00:12:47,760 --> 00:12:51,079
little bit slow.

335
00:12:56,000 --> 00:12:59,760
So, we can see

336
00:12:57,440 --> 00:13:02,399
it's connected to the Windows 10 target

337
00:12:59,760 --> 00:13:04,959
and it's actually showing

338
00:13:02,399 --> 00:13:07,839
symbols. If we continue with the go

339
00:13:04,959 --> 00:13:07,839
command,

340
00:13:09,519 --> 00:13:13,920
the target VM will continue booting. So,

341
00:13:11,839 --> 00:13:16,000
for whatever reason, I've noticed that

342
00:13:13,920 --> 00:13:18,399
sometimes the virtual machine can be

343
00:13:16,000 --> 00:13:20,639
quite slow to boot and one way I found

344
00:13:18,399 --> 00:13:23,760
to actually unblock it was to actually

345
00:13:20,639 --> 00:13:25,360
start Wireshark on that specific vmnet1

346
00:13:23,760 --> 00:13:28,560
interface and you're going to see all

347
00:13:25,360 --> 00:13:31,680
the UDP packets on port 50000 sent by

348
00:13:28,560 --> 00:13:33,440
WinDbg. After you do that

349
00:13:31,680 --> 00:13:35,920
your virtual machine will be booted. So

350
00:13:33,440 --> 00:13:38,399
now the target VM is started, one thing

351
00:13:35,920 --> 00:13:39,519
you want to do is make sure the debugger

352
00:13:38,399 --> 00:13:41,839
is attached.

353
00:13:39,519 --> 00:13:44,639
So we can see we can break,

354
00:13:41,839 --> 00:13:47,519
get the backtrace,

355
00:13:44,639 --> 00:13:49,839
and continue execution,

356
00:13:47,519 --> 00:13:52,079
and then on target VM,

357
00:13:49,839 --> 00:13:56,399
you know it started,

358
00:13:52,079 --> 00:13:56,399
you want to actually do a snapshot of it.

359
00:13:57,120 --> 00:14:01,880
So, booted

360
00:13:58,560 --> 00:14:01,880
with debugger

361
00:14:02,320 --> 00:14:06,480
and the reason for that is here you can

362
00:14:04,320 --> 00:14:08,800
see the green arrow because it's

363
00:14:06,480 --> 00:14:10,639
actually a snapshot when it's booted

364
00:14:08,800 --> 00:14:12,480
and the reason is we can actually do

365
00:14:10,639 --> 00:14:14,240
lots of things

366
00:14:12,480 --> 00:14:18,160
on it.

367
00:14:14,240 --> 00:14:21,199
We can even detach the debugger.

368
00:14:18,160 --> 00:14:24,880
And then here the virtual machine, we can

369
00:14:21,199 --> 00:14:27,279
restore the snapshot, so using that

370
00:14:24,880 --> 00:14:29,040
orange arrow, we can restart the snapshot

371
00:14:27,279 --> 00:14:31,839
to "booted with debugger",

372
00:14:29,040 --> 00:14:31,839
say yes.

373
00:14:35,199 --> 00:14:39,760
Here the virtual machine won't be usable

374
00:14:37,839 --> 00:14:41,519
with that snapshot until we actually

375
00:14:39,760 --> 00:14:43,600
attach with the debugger but the main

376
00:14:41,519 --> 00:14:45,360
advantage is that we can mess up with

377
00:14:43,600 --> 00:14:48,720
the virtual machine and then we don't

378
00:14:45,360 --> 00:14:52,079
have to reboot it entirely.

379
00:14:48,720 --> 00:14:55,199
We can just reattach,

380
00:14:52,079 --> 00:14:56,959
so actually here I can reuse the virtual

381
00:14:55,199 --> 00:14:58,480
machine, even the debugger is not

382
00:14:56,959 --> 00:15:01,040
attached and then I can attach the

383
00:14:58,480 --> 00:15:01,040
debugger.

384
00:15:01,920 --> 00:15:06,720
We see it's connected, but it's actually

385
00:15:03,839 --> 00:15:10,079
not showing more than that.

386
00:15:06,720 --> 00:15:11,519
So, what we can do is hit Break

387
00:15:10,079 --> 00:15:14,639
and now

388
00:15:11,519 --> 00:15:14,639
we're back to debugging.

389
00:15:15,680 --> 00:15:20,399
The thing we can do in WinDbg is just show

390
00:15:21,760 --> 00:15:27,440
like kernel function, see the actual

391
00:15:25,120 --> 00:15:30,000
modules that are being loaded. So

392
00:15:27,440 --> 00:15:34,560
here we can see only ntoskrnl, which

393
00:15:30,000 --> 00:15:36,560
is named nt, is actually shown. So to

394
00:15:34,560 --> 00:15:38,480
reload all the symbols,

395
00:15:36,560 --> 00:15:40,959
we can do .reload /f

396
00:15:38,480 --> 00:15:42,959


397
00:15:40,959 --> 00:15:45,199
and it's going to actually download all

398
00:15:42,959 --> 00:15:47,360
the symbols into

399
00:15:45,199 --> 00:15:50,560
the folder that has been defined which

400
00:15:47,360 --> 00:15:52,240
is in C:\symbols.

401
00:15:50,560 --> 00:15:54,079
So, we're going to see lots of folders

402
00:15:52,240 --> 00:15:56,399
being added here and it's going to take

403
00:15:54,079 --> 00:15:56,399
a while.

404
00:15:56,480 --> 00:16:01,040
Okay, so after a few minutes you'll see

405
00:15:58,399 --> 00:16:03,199
that you have lots of pdb files

406
00:16:01,040 --> 00:16:06,160
corresponding to all the modules,

407
00:16:03,199 --> 00:16:09,199
and reexecuting "lm l"

408
00:16:06,160 --> 00:16:12,880
will actually show you all the

409
00:16:09,199 --> 00:16:12,880
the modules that have symbols.

410
00:16:14,959 --> 00:16:21,160
So, we see we have more symbols

411
00:16:17,279 --> 00:16:21,160
that we used to have before.

412
00:16:26,320 --> 00:16:28,560


413
00:16:30,399 --> 00:16:32,880
Before,

414
00:16:32,959 --> 00:16:37,360
and after.

415
00:16:36,000 --> 00:16:40,320
Okay,

416
00:16:37,360 --> 00:16:42,880
that ends the WinDbg video. Thank you

417
00:16:40,320 --> 00:16:42,880
for watching!

