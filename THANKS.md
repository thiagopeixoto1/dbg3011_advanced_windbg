
I am Cedric Halbronn - [cedric.halbronn@nccgroup.com](mailto:cedric.halbronn@nccgroup.com) / [@saidelike](https://twitter.com/saidelike) - I am the maintainer of this course on OpenSecurityTraining

I would like to thank the following people for helping with this project:

* Aaron Adams - [@FidgetingBits](https://twitter.com/fidgetingbits) for creating the original content with me
* Ollie Whitehouse - [@ollieatnccgroup](https://twitter.com/ollieatnccgroup) for supporting the open-source release of this course on OpenSecurityTraining
* Xeno Kovah - [@XenoKovah](https://twitter.com/XenoKovah) for all the help around creating the best content on OpenSecurityTraining
* Hudson Jacobs - [contact](ursamajormediallc@gmail.com) for editing my videos