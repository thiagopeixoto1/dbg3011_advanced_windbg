All materials in this repository are licensed under a Creative Commons "Share Alike" license
https://creativecommons.org/licenses/by-sa/4.0/

Attribution condition: You must indicate that derivative work "Is derived from Cedric Halbronn's 'Debuggers 3011 - Advanced WinDbg' class, available at https://ost2.fyi"
